import {
    SIGNUP_SUCCESS,
    SIGNUP_FAIL,
    LOGIN_SUCCESS,
    LOGIN_FAIL,
    LOGOUT,
    AUTHENTICATED_FAIL,
    AUTHENTICATED_SUCCESS,
    USER_LOADED_SUCCESS,
    USER_LOADED_FAIL,
    COURIER_LOADED_SUCCESS,
    CATEGORY_SUCCESS,
    INCOMING_SUCCESS,
    COURIER_SUCCESS,
    OUTGOING_SUCCESS 
} from '../actions/types';

const initialState = {
    access: localStorage.getItem('access'),
    refresh: localStorage.getItem('refresh'),
    isAuthenticated: null,
    user: null
};

export default function(state = initialState, action) {
    const { type, payload } = action;

    switch(type) {
        case AUTHENTICATED_SUCCESS:
            return {
                ...state,
                isAuthenticated: true
            }
        case LOGIN_SUCCESS:
            localStorage.setItem('access', payload.access);
            return {
                ...state,
                isAuthenticated: true,
                access: payload.access,
                refresh: payload.refresh
            }
        case USER_LOADED_SUCCESS:
            return { //birdene baxa bilersen  1 deq 
                ...state,
                user: payload
            }
        case SIGNUP_SUCCESS:
            return {
                ...state,
                isAuthenticated: false
            }
        case AUTHENTICATED_FAIL:
            return {
                ...state,
                isAuthenticated: false
            }
        case USER_LOADED_FAIL:
            return {
                ...state,
                user: null
            }
        case SIGNUP_FAIL:
        case LOGIN_FAIL:
        case LOGOUT:
            localStorage.removeItem('access');
            localStorage.removeItem('refresh');
            return {
                ...state,
                access: null,
                refresh: null,
                isAuthenticated: false,
                user: null
            };

        case COURIER_LOADED_SUCCESS:
            return {
                ...state,
                courier: payload,
            };
        
            case USER_LOADED_SUCCESS:
            return {
                ...state,
                user: payload,
            };

        case CATEGORY_SUCCESS:
            return {
                ...state,
                category: payload
            };
        
            case INCOMING_SUCCESS:
                return {
                ...state,
                incoming: payload
            };
            case COURIER_SUCCESS:
                return {
                ...state,
                courier: payload
            };
            case OUTGOING_SUCCESS:
                return {
                ...state,
                outgoing: payload
            };
            
        default:
            return state
    }
}
