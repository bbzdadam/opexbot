import React from 'react'
import { Nav } from 'react-bootstrap';
import { Route, Switch, BrowserRouter, NavLink } from 'react-router-dom';
import Dashboard from '../Dashboard/Dashboard';
import Login from './Login'
class NotFound extends React.Component {
    render() {
        return (
            <div className="notfoundscreen">
                <img src="https://res.cloudinary.com/dgslkvls9/image/upload/v1603176020/404not_qa4yzl.svg" className="upsnotfoundimg"></img>
                <NavLink to="/"><button className="backtohome">Back to main page</button></NavLink>
            </div>
        );
    }
}

export default NotFound