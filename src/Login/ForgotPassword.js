import React, {useState} from 'react';
import { NavLink, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { reset_password } from '../actions/auth';

const ForgotPassword = (props) => {
    const [requestSent, setRequestSent] = useState(false);

    const [formData, setFormData] = useState({
        email: ''
    });

    const { email } = formData;

    const onChange = e => setFormData({ ...formData, [e.target.name]: e.target.value });

    const onSubmit = e => {
        e.preventDefault();

        props.reset_password(email);
        setRequestSent(true);
    };

    if (requestSent)
        return <Redirect to='/mailrecoverysuccess' />
        return (
            <div className="landingpage">
                <div className="loginleft">
                    <img className="logoindex" src="https://res.cloudinary.com/dgslkvls9/image/upload/v1600328350/group-23_3x_azx7iv.png"></img>
                    <img className="landingimg" src="https://res.cloudinary.com/dgslkvls9/image/upload/v1600328351/group-280_3x_ikbi1o.png"></img>
                        <div className="footerlinks">
                            <a className="termsofuse">Terms of use </a>
                            <a>|</a>
                            <a className="privacypolicy"> Privacy policy</a>
                        </div>
                </div>

                <div className="loginright">
                    <div className="recoverpage">
                        <NavLink to="/" ><button className="gobacklogin">🡨 Back</button></NavLink>
                        <p className="pswrdrecov">Password recovery</p>
                        <form onSubmit={e => onSubmit(e)}>
                            <div className="emailrecoverycl">
                                <input type="email" placeholder="Email" className="recoverinputemail" name='email' value={email}  onChange={e => onChange(e)} required></input>
                            </div>
                            <button type="submit" className="submitrecov">Submit</button>
                        </form>
                    </div>

                </div>
            </div>
        );
    }


    export default connect(null, { reset_password })(ForgotPassword);
