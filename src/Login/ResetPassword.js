import React, {useState} from 'react';
import { NavLink, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { reset_password } from '../actions/auth';


function showPassword(){
    var pass = document.getElementById("Password");
    if (pass.type === "password") {
        pass.type = "text";
    }
    else {
        pass.type = "password";
    }
}


const ForgotPassword = (props) => {
    const [requestSent, setRequestSent] = useState(false);

    const [formData, setFormData] = useState({
        new_password: '',
        re_new_password: ''
    });

    const { new_password, re_new_password } = formData;

    const onChange = e => setFormData({ ...formData, [e.target.name]: e.target.value });

    const onSubmit = e => {
        e.preventDefault();
        
        const uid = props.match.params.uid;
        const token = props.match.params.token;

        props.reset_password_confirm(uid, token, new_password, re_new_password);
        setRequestSent(true);
    };

    if (requestSent)
        return <Redirect to='/passwordchangesucced' />
        return (
            <div className="landingpage">
                <div className="loginleft">
                    <img className="logoindex" src="https://res.cloudinary.com/dgslkvls9/image/upload/v1600328350/group-23_3x_azx7iv.png"></img>
                    <img className="landingimg" src="https://res.cloudinary.com/dgslkvls9/image/upload/v1600328351/group-280_3x_ikbi1o.png"></img>
                        <div className="footerlinks">
                            <a className="termsofuse">Terms of use </a>
                            <a>|</a>
                            <a className="privacypolicy"> Privacy policy</a>
                        </div>
                </div>

                <div className="loginright">
                    <div className="resetpage">
                        <p className="pswrdrecov">Reset password</p>
                        <form onSubmit={e => onSubmit(e)}>
                            <div className="passwordcredentialreset">
                                <input  id="Password" name='new_password' value={new_password} required onChange={e => onChange(e)} type="password" className="enterpasscred" placeholder="Password"></input>
                                <img onClick={showPassword} className="showpassword" src="https://res.cloudinary.com/dgslkvls9/image/upload/v1600337615/visibility-off-material_3x_nqp7qb.png"></img>
                            </div>
                            <div className="passwordcredentialreset">
                                <input  id="Password" name='re_new_password' value={re_new_password} required onChange={e => onChange(e)} type="password" className="enterpasscred" placeholder="Password"></input>
                                <img onClick={showPassword} className="showpassword" src="https://res.cloudinary.com/dgslkvls9/image/upload/v1600337615/visibility-off-material_3x_nqp7qb.png"></img>
                            </div>

                            <button type="submit" className="submitreset">Submit</button>
                        </form>
                    </div>

                </div>
            </div>
        );
    }


    export default connect(null, { reset_password })(ForgotPassword);
