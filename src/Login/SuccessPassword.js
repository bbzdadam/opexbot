import React from 'react'
import {NavLink} from 'react-router-dom'
class Success extends React.Component {
    render() {
        return (
            <div className="landingpage">
            <div className="loginleft">
                <img className="logoindex" src="https://res.cloudinary.com/dgslkvls9/image/upload/v1600328350/group-23_3x_azx7iv.png"></img>
                <img className="landingimg" src="https://res.cloudinary.com/dgslkvls9/image/upload/v1600328351/group-280_3x_ikbi1o.png"></img>
                    <div className="footerlinks">
                        <a className="termsofuse">Terms of use </a>
                        <a>|</a>
                        <a className="privacypolicy"> Privacy policy</a>
                    </div>
            </div>

            <div className="loginright">
                <div className="recoverpagesucced">
                    <div className="checkedimg">
                        <img src="https://res.cloudinary.com/dgslkvls9/image/upload/v1603182558/check_ecupx6.png"></img>
                    </div>
                    <p className="checkemail">Congratulations!</p>
                    <p className="mailinstructions">Your password successfully changed.</p>
                    <NavLink to="/"><button className="submitreset">Log in</button></NavLink>

                </div>

            </div>
        </div>
        );
    }
}

export default Success