import React from "react";
import { Redirect, NavLink } from "react-router-dom";
import { connect } from 'react-redux'
import { login } from '../actions/auth'

function showPassword(){
    var pass = document.getElementById("Password");
    if (pass.type === "password") {
        pass.type = "text";
    }
    else {
        pass.type = "password";
    }
}

const Login = ({ login, isAuthenticated }) => {
    const [formData, setFormData] = React.useState({
      email: "",
      password: "",
    });
  
    const { email, password } = formData;
  
    const onChange = (e) =>
      setFormData({ ...formData, [e.target.name]: e.target.value });
  
    const onSubmit = (e) => {
      e.preventDefault();
  
      login(email, password);
    };
  
    if (isAuthenticated) return <Redirect to="/dashboard" />;
    
    return (
            <div className="landingpage">
                <div className="loginleft">
                    <img className="logoindex" src="https://res.cloudinary.com/dgslkvls9/image/upload/v1600328350/group-23_3x_azx7iv.png"></img>
                    <img className="landingimg" src="https://res.cloudinary.com/dgslkvls9/image/upload/v1600328351/group-280_3x_ikbi1o.png"></img>
                        <div className="footerlinks">
                            <a className="termsofuse">Terms of use </a>
                            <a>|</a>
                            <a className="privacypolicy"> Privacy policy</a>
                        </div>
                </div>

                <div className="loginright">
                    <h5 className="loginhead" >Log in to your account</h5>
                    {/*<div id="or">or</div>*/}
                      <form className="logincredentials_input" onSubmit={(e) => onSubmit(e)}>                
                            <div className="emailcredential">
                                <input 
                                type="email" 
                                name="email"
                                required 
                                value={email}
                                onChange={(e) => onChange(e)} 
                                className="enteremailcred" 
                                placeholder="Email">
                                </input>
                            </div>

                            <div className="passwordcredential">
                                <input  id="Password" name="password" value={password} required onChange={(e) => onChange(e)} type="password" className="enterpasscred" placeholder="Password"></input>
                                <img onClick={showPassword} className="showpassword" src="https://res.cloudinary.com/dgslkvls9/image/upload/v1600337615/visibility-off-material_3x_nqp7qb.png"></img>
                            </div>

                            <div className="loginsettings">
                                <div className="rememberme">
                                    <input type="checkbox" className="remembercheck"></input>
                                    <p className="textofremember">Remember me?</p>
                                </div>
                                <NavLink to="/forgetpass"><p className="uforgotpass">Forgot password?</p></NavLink>
                            </div>

                        <button className="signin" type="submit" >Login</button>

                    </form>

                </div>
            </div>
    );
};

const mapStateToProps = (state) => ({
    isAuthenticated: state.auth.isAuthenticated,
});
      
export default connect(mapStateToProps, { login })(Login);