import React from 'react'
import 'moment/locale/az.js'
import 'rc-datepicker/lib/style.css';
import { DatePickerInput, DatePicker } from 'rc-datepicker';
import Docs from './Docs';



const date = new Date();
const onChange = (jsDate, dateString) => {
    // ...
  }

class Elements extends React.Component {

    state = {
        post: [
          { id: 1, 
            fullname: "Adam Baba", 
            nid: "003", 
            phnumber:"+994517117525", 
            time:"14:00", 
            location:"Ehmedli", 
            totalprice:"28,50", 
            comment: "Soğan vırmiyun, mayanezi bol olsun alergiyam var gic olaram. lorem ipsum dolor is ",
            count: 2, 
            countes: 4,
            drink: "Coca Cola",
            food: "Cheeseburger Menu",
            fooddetails: "Cheeseburger, French fries",
            types: "Delivery",
            rating: "https://res.cloudinary.com/dgslkvls9/image/upload/v1602665567/Group_92_3x_pvc3wx.png"
            },            
        ]
      };



    render() {
        
        return (
            <div>
                <div className="searcharchive">
                    <div className="searchbyname">
                        <img src="https://res.cloudinary.com/dgslkvls9/image/upload/v1602497333/Search_Loupe_etcc9x.png" className="searchicon"></img>
                        <input placeholder="Axtarış et" className="searchinput"></input>
                    
                    </div>
                    <div className="searchbydate">
                        <DatePickerInput
                            onChange={onChange}
                            value={date}
                            className='my-custom-datepicker-component'
                             
                        />
                    </div>
                </div>
                <div className="archivecatsdiv">
                    <div className="archivecats">
                        <p className="musteri">MÜŞTƏRİ</p>  
                        <p className="musterielaqe">ƏLAQƏ</p>   
                        <p className="sifarisdeyer">DƏYƏRLƏNDİRMƏ</p> 
                        <p className="sifarisqiymet">QIYMƏT</p> 
                        <p className="sifarisvaxt">VAXT</p> 
                        <p className="sifaristarix">TARIX</p> 
                    </div>
                </div>
                <div className="allarchivedocs">
                    <Docs  data={this.state.post}/> 
                </div>
            </div>
        );
    }
}

export default Elements