import React, { Component } from "react";


function hide() {
  var x = document.getElementById("archivemodal");
  if (x.style.display === "block") {
	  x.style.display = "none";

    } else {
	  x.style.display = "none";
    }
}


class ArchiveModal extends Component {

	constructor(){
		super()
		this.state = {
			delivery: true	
		}
	}


  render() {

	let orderType
  let ratingSrc

	if(this.state.delivery === true){
		orderType = "https://res.cloudinary.com/dgslkvls9/image/upload/v1598770300/delivery_3x_bbxxjy.png"
	} else {
		orderType = "https://res.cloudinary.com/dgslkvls9/image/upload/v1598770299/cars_3x_k9akn6.png"
  }

    console.log(this.props.show);

    return (
      <React.Fragment>
        {this.props.show && (
          <div id="archivemodal" className="archivemodal">
            <div className="btnx">
              <button className="closebt" onClick={this.props.onHide}><img  className="clsbtnmg" src="https://res.cloudinary.com/dgslkvls9/image/upload/v1600776523/Add_Cirlce_Plus_4_3x_s4bc4m.png"></img></button>
            </div>
            <div class="popuprect">
              <div id="popu8pwind" class="detallar">
                <div className="archivemodalpoint">
                  <p className="archiveordid">#{this.props.nid}</p>
                  <p className="musterifull">{this.props.fullname}</p>
                  <img className="orderdeltyp" src={orderType}></img>
                </div>
                <div className="modal_point2">
                  <p className="archiveordphnom">{this.props.phnumber}</p>
                  <p className="musteriunvan">{this.props.location}.</p>
                  <div className="muddetrate">
                    <p className="hazirlanmamuddet">Kuryerin Fealiyyeti</p>
                    <img className="thumbsdup" src={ratingSrc}></img>
                  </div>
                  <div className="muddetrate">
                    <p className="hazirlanmamuddetyemek">Yemek </p> 
                    <img className="thumbsdup" src={ratingSrc}></img>
                  </div>
                  <p className="hazirlanmamuddet">Hazirlanma Muddeti:   deq </p>
                  <p className="hazirlanmamuddet">Catdirilma Muddeti:  deq </p>
                </div>
                <hr></hr>
                <div class="orderdetails_1">
                  <p class="count">{this.props.count}x</p>
                  <p class="foodname">{this.props.food}</p>
                </div>
                <div class="dtl">	
								  <p class="fooddetails"> {this.props.fooddetails}</p>
                  </div>
                  		
                <div class="orderdetails_2">
                  <p class="countes">{this.props.countes}x</p>
                  <p class="foodname">{this.props.drink}</p>
                </div>	
                <div class="price">
                  <p class="total">{this.props.totalprice	} AZN</p>
                </div> 
              </div>
            </div>  
          </div>
        )}
      </React.Fragment>
    );
  }
}

export default ArchiveModal;
