import React from 'react'
import HeaderArchive from '../Main/HeaderArchive'
import SidebarArchive from '../Main/SidebarArchive'
import Elements from './Elements'

class Archive extends React.Component {
    render() {
        return (
            <div className="testgorek">
                <HeaderArchive/>
                <SidebarArchive/>
                <div className="archivebody">
                    <Elements/>
                </div>
            </div>
        );
    }
}

export default Archive