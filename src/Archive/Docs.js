import React from 'react'
import ArchiveModal from './ArchiveModal';

class Docs extends React.Component {
    
    constructor(){
		super()
		this.state = {
            delivery: true,
            rating: 3,  	
		}
	}

    state = {
        showModal: 0
    };
    
    getModal = value => {
        this.setState({ showModal: value });
    };
    
    hideModal = value => {
        this.setState({ showModal: 0 });
    };


    render() {
        let ratingSrc

        if(this.state.rating == 1) {
            ratingSrc="https://res.cloudinary.com/dgslkvls9/image/upload/v1602665567/Group_89_3x_ztkayy.png"
        }else if(this.state.rating == 2) {
            ratingSrc="https://res.cloudinary.com/dgslkvls9/image/upload/v1602665566/Group_90_3x_lma8rh.png"
        }else if(this.state.rating == 3) {
            ratingSrc="https://res.cloudinary.com/dgslkvls9/image/upload/v1602665566/Group_91_3x_ac5zcg.png"
        }else if(this.state.rating == 4) {
            ratingSrc="https://res.cloudinary.com/dgslkvls9/image/upload/v1602665567/Group_92_3x_pvc3wx.png"
        }else {
           ratingSrc= "https://res.cloudinary.com/dgslkvls9/image/upload/v1602665566/Group_87_3x_dcwgf9.png"  
           
        }
        return (
        <div>
            {this.props.data.map((data, key)=> (
                <div className="sifarisdetalarxiv">
                    <div className="sifarisdetalarxiv" onClick={() => this.getModal(data.id)}>
                        <p className="archiveordid">#{data.nid}</p>
                        <div className="archiveordmusteri">
                            <p className="musterifull">{data.fullname}</p>
                            <p className="musteriunvan">{data.location}.</p>
                        </div>
                        <p className="archiveordphnom">{data.phnumber}</p>
                        <div className="archiveorddeyer">
                            <img className="thumbsup" src={ratingSrc}></img>
                        </div>
                        <p className="archiveordqiymet">{data.totalprice} ₼</p>
                        <p className="archiveordvaxt">32 dəq.</p>
                        <p className="archiveordtarix">20.09.2020  14:25</p>
                    </div>
                    
                    <ArchiveModal
                        show={this.state.showModal === data.id}
                        onHide={() => this.hideModal(data.id)}
                        nid={data.nid}
                        time={data.time}
                        fullname={data.fullname}
                        phnumber={data.phnumber}
                        location={data.location}
                        totalprice={data.totalprice}
                        comment={data.comment}
                        count={data.count}
                        countes={data.countes}
                        drink={data.drink}
                        food={data.food}
                        fooddetails={data.fooddetails}
                        types={data.types}
                        timesc={data.timesc}
                        rating={data.rating}
                    />
                    
                </div>
                
            ))}
        </div>  
        
        );
    }
}

export default Docs