import React from 'react'
import {courier_update} from '../actions/auth'
import {connect} from 'react-redux';

const ModalElements  = ({ courier_update }) => {
    const [formData, setFormData] = React.useState({
      name: "",
      surname: "",
      phone_number: "",
      id: ""
    });
  
    const { name, surname, phone_number, id } = formData;
  
    const onChange = (e) =>
      setFormData({ ...formData, [e.target.name]: e.target.value });
  
    const onSubmit = (e) => {
      e.preventDefault();
  
      courier_update(name, surname, phone_number, id);
    };
    
    return (
        <div>
            <div id="editkuriyer" className="addkuryerpopup">
                <div className="modalprof">
                    <div className="btnx">
                         <button className="closebt" onClick={this.props.onHide}><img className="clsbtnmg" src="https://res.cloudinary.com/dgslkvls9/image/upload/v1600776523/Add_Cirlce_Plus_4_3x_s4bc4m.png"></img></button>
                    </div>
                    <div class="popuprectkuriyer"  onSubmit={(e) => onSubmit(e)}>
                        <p classname="kuryerlihead">Kuriyer tənzimlə</p>
                        <div classname="kuryercred">
                            <input type="textarea" placeholder={this.props.name} name="name" value={name} onChange={(e) => onChange(e)} className="panelprflinput"></input>
                            <input type="textarea" placeholder={this.props.surname} name="surname" value={surname} onChange={(e) => onChange(e)} className="panelprflinput"></input>
                            <input type="textarea" placeholder={this.props.phnumber} name="phone_number" value={phone_number} onChange={(e) => onChange(e)} className="panelprflinput"></input>
                            <input className="idnoshow" type="textarea" value={this.props.id}  onChange={(e) => onChange(e)}  ></input>
                        </div>
                        <button classname="elavekuryer" type="submit" onClick={this.props.onHide}>Əlavə et</button>
                    </div>                   
                </div>
            </div>
        </div>   
    );
}
  
  export default connect(null, { courier_update })(ModalElements);