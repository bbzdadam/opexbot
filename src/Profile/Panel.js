import React from 'react';
import { load_user } from '../actions/auth'
import { connect } from 'react-redux';
import PanelCatgs from './PanelCatgs';
import NewKuriyer from './NewKuriyer';
import ProfileUpdate from './ProfileUpdate';


function openm() {
    var x = document.getElementById("yenim");
    if (x.style.display === "none") {
        x.style.display = "block";
    } else {
        x.style.display = "none";
    }
}



class Panel extends React.Component {

    constructor(props) {
        super();
        props.load_user();        
    }

    

    componentDidMount() {
    }
    

    render() {
        if (!this.props.user) return 'Loading';
        if (!this.props.user.user) return 'Loading';
        let user = this.props.user.user;
        


        return (
            <div className="panelboy">

                <div className="panelboyelements">
                    <div className="panelboyuser">
                        <img className="personalprflimg" src="https://res.cloudinary.com/dgslkvls9/image/upload/v1601292541/Group_46_ovxkfd.png"></img>
                        <div className="personalcred">
                            <p className="adcred">{user.name} {user.surname}</p>
                            <p className="restadcred">{user.username}</p>
                        </div>
                        <div classname="editrest">
                            <img onClick={openm} className="editperson" src="https://res.cloudinary.com/dgslkvls9/image/upload/v1600434247/Pen_Edit.8_1_akastz.png"></img>

                        </div>
                    </div>

                    <PanelCatgs/>
                </div>

                <ProfileUpdate />

                <NewKuriyer/>




            </div>
        );
    }
}
export default connect(state => ({user: state.auth}), {load_user})(Panel)

