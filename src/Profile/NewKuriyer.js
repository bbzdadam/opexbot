import React from 'react'
import {connect} from 'react-redux'
import {courier_create} from '../actions/auth'
import {Redirect} from 'react-router-dom'

function closem() {
    var x = document.getElementById("yenim");
    var y = document.getElementById("yenikuryer");
    if (x.style.display === "block" || y.style.display === "block") {
        x.style.display = "none";
        y.style.display = "none";
    } else {
        x.style.display = "none";
        y.style.display = "none";
    }
}



const NewKuriyer = ({ courier_create, isAuthenticated }) => {
    const [formData, setFormData] = React.useState({
      name: "",
      surname: "",
      phone_number: "",
    });
  
    const { name, surname, phone_number } = formData;
  
    const onChange = (e) =>
      setFormData({ ...formData, [e.target.name]: e.target.value });
  
    const onSubmit = (e) => {
      e.preventDefault();
  
      courier_create(name, surname, phone_number);
    };
  
    if (isAuthenticated) return <Redirect to="/profile" />;
    
    return (
             <div>
                 <div id="yenikuryer" className="addkuryerpopup">
                    <div className="modalprof">
                        <div className="btnx">
                            <button className="closebt" onClick={closem}><img className="clsbtnmg" src="https://res.cloudinary.com/dgslkvls9/image/upload/v1600776523/Add_Cirlce_Plus_4_3x_s4bc4m.png"></img></button>
                        </div>
                        <div class="popuprectkuriyer">
                            <p className="kuryerlihead">Kuriyer yarat</p>
                            <form onSubmit={(e) => onSubmit(e)} > 
                            <div className="kuryercred">
                                <input type="textarea" name="name" required value={name} onChange={(e) => onChange(e)} placeholder="Ad" className="panelprflinput"></input>
                                <input type="textarea" name="surname" required value={surname} onChange={(e) => onChange(e)} placeholder="Soyad" className="panelprflinput"></input>
                                <input type="textarea" name="phone_number" required value={phone_number} onChange={(e) => onChange(e)} placeholder="Nomre" className="panelprflinput"></input>
                            </div>
                            <button className="elavekuryer" type="submit" onClick={closem}>Əlavə et</button>
                            </form>
                        </div>
                    </div>
                </div>
             </div>
        );
    }


          
export default connect(null, { courier_create })(NewKuriyer);