import React from 'react'
import Kuriyer from './Kuriyer';
import {get_courier} from '../actions/auth';
import {connect} from 'react-redux';
import TimeInput from 'react-time-input';

function openkur() {
    var x = document.getElementById("yenikuryer");
    if (x.style.display === "none") {
        x.style.display = "block";
    } else {
        x.style.display = "none";
    }
}

function dropmore() {
    var x = document.getElementById("kuryerlerbody");
    var y = document.getElementById("kuryerdropdown");
    var z = document.getElementById("kuryerdropup");
    var d = document.getElementById("issaatbody");
    if (x.style.display === "none" && y.style.display === "block" && z.style.display === "none" || d.style.display === "block") {
        x.style.display = "block";
        y.style.display = "none";
        z.style.display = "block";
        d.style.display = "none";
    } else {
        x.style.display = "none";
        y.style.display = "block";
        z.style.display = "none";
        d.style.display = "none";
    }

}

function dropless() {
    var x = document.getElementById("kuryerlerbody");
    var y = document.getElementById("kuryerdropdown");
    var z = document.getElementById("kuryerdropup");
    var d = document.getElementById("issaatbody");
    if (x.style.display === "block" && y.style.display === "none" && z.style.display === "block") {
        x.style.display = "none";
        y.style.display = "block";
        z.style.display = "none";
    } else {
        x.style.display = "block";
        y.style.display = "none";
        z.style.display = "block";
    }
}

function dropwrkmore() {
    var x = document.getElementById("issaatbody");
    var y = document.getElementById("workhoursdown");
    var z = document.getElementById("workhoursup");
    var d = document.getElementById("kuryerlerbody");
    if (x.style.display === "none" && y.style.display === "block" && z.style.display === "none" || d.style.display === "block") {
        x.style.display = "block";
        y.style.display = "none";
        z.style.display = "block";
        d.style.display = "none";
    } else {
        x.style.display = "none";
        y.style.display = "block";
        z.style.display = "none";
        d.style.display = "none";

    }

}

function dropwrkless() {
    var x = document.getElementById("issaatbody");
    var y = document.getElementById("workhoursdown");
    var z = document.getElementById("workhoursup");
    var d = document.getElementById("kuryerlerbody");

    if (x.style.display === "block" && y.style.display === "none" && z.style.display === "block") {
        x.style.display = "none";
        y.style.display = "block";
        z.style.display = "none";
    } else {
        x.style.display = "block";
        y.style.display = "none";
        z.style.display = "block";
    }
}


class PanelCatgs extends React.Component{
    

    constructor(props) {
        super();
        props.get_courier();
    };    

    

    

    

    render() {   

        var arr = this.props.courier;
        if (!arr) return 'Loading';


        return (

            <div className="panelcatgs">

                <div className="panelkuryer">
                    <div className="kuryerlerhead">
                        <p>Kuryerler</p>
                        <img onClick={openkur} className="kuryeradd" src="https://res.cloudinary.com/dgslkvls9/image/upload/v1600434247/Add_Cirlce_Plus_3_bc4qfd.png"></img>
                        <img id="kuryerdropdown" onClick={dropmore} className="kuryerdropdown" src="https://res.cloudinary.com/dgslkvls9/image/upload/v1602076412/Arrow.2_2_xlk8w2.png"></img>
                        <img id="kuryerdropup" onClick={dropless} className="kuryerdropup" src="https://res.cloudinary.com/dgslkvls9/image/upload/v1602076334/Arrow.2_1_1_bk6rfi.png"></img>
                    </div>

                    <div id="kuryerlerbody" className="kuryerlerbody">
                        <div className="kuryerlercategory">
                            <p className="kryad">Ad Soyad</p>
                            <p className="krynom">Nömrə</p>
                            <p className="krystat">Status</p>
                        </div>
                        <Kuriyer courier={this.props.courier}/> 
                    </div>
                </div>

            


                <div className="panelissaati">
                    <div className="issaathead">
                        <p>İş saatları</p>
                        <img id="workhoursdown" onClick={dropwrkmore} className="kuryerdropdown" src="https://res.cloudinary.com/dgslkvls9/image/upload/v1602076412/Arrow.2_2_xlk8w2.png"></img>
                        <img id="workhoursup" onClick={dropwrkless} className="kuryerdropup" src="https://res.cloudinary.com/dgslkvls9/image/upload/v1602076334/Arrow.2_1_1_bk6rfi.png"></img>

                    </div>

                    <div id="issaatbody" className="issaatbody">
                        <div className="heftegunleri">
                            <p>Bazar ertest</p>
                            <div className="saatsec">
                            <TimeInput
                                initTime='11:12'
                                ref="TimeInputWrapper"
                                className='form-control'
                                mountFocus='true'
                                onTimeChange={this.onTimeChangeHandler}
                            />
                            </div>
                            
                            <div className="saatsec">
                            <TimeInput
                                initTime='11:12'
                                ref="TimeInputWrapper"
                                className='form-control'
                                mountFocus='true'
                                onTimeChange={this.onTimeChangeHandler}
                            />
                            </div>
                            <input className="radioinput" type="radio"></input>
                        </div>
                        <div className="heftegunleri">
                            <p>Cersenbe axsami</p>
                            <div className="saatsec">
                            <TimeInput
                                initTime='11:12'
                                ref="TimeInputWrapper"
                                className='form-control'
                                mountFocus='true'
                                onTimeChange={this.onTimeChangeHandler}
                            />
                            </div>
                            <div className="saatsec">
                            <TimeInput
                                initTime='11:12'
                                ref="TimeInputWrapper"
                                className='form-control'
                                mountFocus='true'
                                onTimeChange={this.onTimeChangeHandler}
                            />
                            </div>
                            <input className="radioinput" type="radio"></input>
                        </div>
                        <div className="heftegunleri">
                            <p>Cersenbe</p>
                            <div className="saatsec">
                            <TimeInput
                                initTime='11:12'
                                ref="TimeInputWrapper"
                                className='form-control'
                                mountFocus='true'
                                onTimeChange={this.onTimeChangeHandler}
                            />
                            </div>
                            <div className="saatsec">
                            <TimeInput
                                initTime='11:12'
                                ref="TimeInputWrapper"
                                className='form-control'
                                mountFocus='true'
                                onTimeChange={this.onTimeChangeHandler}
                            />
                            </div>
                            <input className="radioinput" type="radio"></input>
                        </div>
                        <div className="heftegunleri">
                            <p>Cume axsami</p>
                            <div className="saatsec">
                            <TimeInput
                                initTime='11:12'
                                ref="TimeInputWrapper"
                                className='form-control'
                                mountFocus='true'
                                onTimeChange={this.onTimeChangeHandler}
                            />
                            </div>
                            <div className="saatsec">
                            <TimeInput
                                initTime='11:12'
                                ref="TimeInputWrapper"
                                className='form-control'
                                mountFocus='true'
                                onTimeChange={this.onTimeChangeHandler}
                            />
                            </div>
                            <input className="radioinput" type="radio"></input>
                        </div>
                        <div className="heftegunleri">
                            <p>Cume</p>
                            <div className="saatsec">
                            <TimeInput
                                initTime='11:12'
                                ref="TimeInputWrapper"
                                className='form-control'
                                mountFocus='true'
                                onTimeChange={this.onTimeChangeHandler}
                            />
                            </div>
                            <div className="saatsec">
                            <TimeInput
                                initTime='11:12'
                                ref="TimeInputWrapper"
                                className='form-control'
                                mountFocus='true'
                                onTimeChange={this.onTimeChangeHandler}
                            />
                            </div>
                            <input className="radioinput" type="radio"></input>
                        </div>
                        <div className="heftegunleri">
                            <p>Senbe</p>
                            <div className="saatsec">
                            <TimeInput
                                initTime='11:12'
                                ref="TimeInputWrapper"
                                className='form-control'
                                mountFocus='true'
                                onTimeChange={this.onTimeChangeHandler}
                            />
                            </div>
                            <div className="saatsec">
                            <TimeInput
                                initTime='11:12'
                                ref="TimeInputWrapper"
                                className='form-control'
                                mountFocus='true'
                                onTimeChange={this.onTimeChangeHandler}
                            />
                            </div>
                            <input className="radioinput" type="radio"></input>
                        </div>
                        <div className="heftegunleri">
                            <p>Bazar </p>
                            <div className="saatsec">
                            <TimeInput
                                initTime='11:12'
                                ref="TimeInputWrapper"
                                className='form-control'
                                mountFocus='true'
                                onTimeChange={this.onTimeChangeHandler}
                            />
                            </div>
                            <div className="saatsec">
                            <TimeInput
                                initTime='11:12'
                                ref="TimeInputWrapper"
                                className='form-control'
                                mountFocus='true'
                                onTimeChange={this.onTimeChangeHandler}
                            />
                            </div>
                            <input className="radioinput" type="radio"></input>
                        </div>
                    </div>
                </div>


            </div>
        );
    }
}


export default connect(state => ({courier: state.auth.courier}), {get_courier}) (PanelCatgs)
