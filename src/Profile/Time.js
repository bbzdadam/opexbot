import React from 'react';
import TimeInput from 'react-time-input';
 
class Time extends React.Component {
 
   render() {
   	return (
   		<TimeInput
   			initTime='11:12'
   			ref="TimeInputWrapper"
   			className='form-control'
   			mountFocus='true'
   			onTimeChange={this.onTimeChangeHandler}
   		/>
   	);
   }
};

export default Time
