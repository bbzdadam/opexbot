import React from 'react'
import Sidebarprof from '../Main/Sidebarprof'
import Headerprof from '../Main/Headerprof'
import Panel from './Panel'


const Profilepanel = () => {

    return (
        <div>
            <Sidebarprof />
            <Headerprof />
            <div className="profilepanelbody">
               <Panel/>
            </div>  

      
        </div>
    );
}

export default Profilepanel