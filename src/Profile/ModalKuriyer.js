import React from 'react'
import {courier_update, courier_delete} from '../actions/auth'
import {connect} from 'react-redux'


function closem() {
    var x = document.getElementById("editkuriyer");
    if (x.style.display === "block") {
        x.style.display = "none";
    } else {
        x.style.display = "none";
    }
}

class ModalKuriyer extends React.Component {

    constructor(props) {
        super(); 
        this.state={
            username: "",
            surname: "",
            phone_number: "",
            id: ""
        };


    }
    onChange = (e) => {
        this.setState({
            ...this.state,
            [e.target.name]: e.target.value 
        });
    }

    onSubmit = (e) => {
        e.preventDefault();
        // this.props.courier_update( this.state.username, this.state.surname, this.state.phone_number, this.state.id);
        alert("Submitted : " + this.state.username + this.state.surname + this.state.phone_number + this.state.id)
    }

    onDelete =(e) => {
        e.preventDefault();
        this.props.courier_delete( this.state.username, this.state.surname, this.state.phone_number, this.state.id);


    }



    render() {

        return (
            <React.Fragment>
            {this.props.show &&(
            <div>
                <div id="editkuriyer" className="addkuryerpopup">
                    <div className="modalprof">
                        <div className="btnx">
                             <button className="closebt" onClick={this.props.onHide}><img className="clsbtnmg" src="https://res.cloudinary.com/dgslkvls9/image/upload/v1600776523/Add_Cirlce_Plus_4_3x_s4bc4m.png"></img></button>
                        </div>
                        
                        <div class="popuprectkuriyer" >
                            <form onSubmit={(e) => this.onSubmit(e)}>
                                <p classname="kuryerlihead">Kuriyer tənzimlə</p>
                                <div classname="kuryercred">
                                    <input type="textarea" required name="username" value={this.state.username} onChange={(e) => this.onChange(e)} placeholder={this.props.name} className="panelprflinput"></input>
                                    <input type="textarea" required name="surname" value={this.state.surname} onChange={(e) => this.onChange(e)} placeholder={this.props.surname}className="panelprflinput"></input>
                                    <input type="textarea" required name="phone_number" value={this.state.phone_number} onChange={(e) => this.onChange(e)} placeholder={this.props.phnumber} className="panelprflinput"></input>
                                    <input className="idshow"  name="id" value={this.props.id} onChange={(e) => this.onChange(e)}   placeholder={this.props.id} type="textarea"  ></input>
                                </div>
                                <button classname="elavekuryer" type="submit" onClick={closem} >Əlavə et</button>
                            </form>  
                        </div>                  
                    </div>
                </div>
            </div>
            )}
            </React.Fragment>
        );
    }
}

export default connect(null,{ courier_update })(ModalKuriyer);