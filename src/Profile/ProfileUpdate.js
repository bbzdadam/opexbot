import React from 'react'
import {connect} from 'react-redux'
import {profile_update} from '../actions/auth'
import {Redirect} from 'react-router-dom'
import UpdatePass from './UpdatePass';




function showpass() {
    var x = document.getElementById("poppassword");
    if (x.style.display === "none") {
        x.style.display = "block";
    } else {
        x.style.display = "none";
    }
}


function closem() {
    var x = document.getElementById("yenim");
    var y = document.getElementById("yenikuryer");
    if (x.style.display === "block" || y.style.display === "block") {
        x.style.display = "none";
        y.style.display = "none";
    } else {
        x.style.display = "none";
        y.style.display = "none";
    }
}


const ProfileUpdate  = ({ profile_update, isAuthenticated }) => {

    const [formData, setFormData] = React.useState({
        name: "",
        surname: "",
        username: "",
      });
    
      const { name, surname, username } = formData;
    
      const onChange = (e) =>
        setFormData({ ...formData, [e.target.name]: e.target.value });
    
      const onSubmit = (e) => {
        e.preventDefault();

        profile_update(name, surname, username);
      };
    
      if (isAuthenticated) return <Redirect to="/profile" />;

        return (
            <div>
                 <div id="yenim" className="editprofilpopup">
                    <div className="modalprof">
                        <div className="btnx">
                            <button className="closebt" onClick={closem}><img className="clsbtnmg" src="https://res.cloudinary.com/dgslkvls9/image/upload/v1600776523/Add_Cirlce_Plus_4_3x_s4bc4m.png"></img></button>
                        </div>
                        <form onSubmit={(e) => onSubmit(e)}>
                        <div class="popuprectprof">
                            <div className="popupuser">
                                <img className="popprflimg" src="https://res.cloudinary.com/dgslkvls9/image/upload/v1601292541/Group_46_ovxkfd.png"></img>
                                <p className="profileimgstatus">Şəkili dəyiş</p>
                                <p className="profileimgstatusadd">Şəkil əlavə et</p>
                            </div>
                            <div className="popupusdet">
                                <input type="textarea" name="name" value={name} onChange={(e) => onChange(e)} placeholder="Ad" className="panelprflinput"></input>
                                <input type="textarea" name="surname" value={surname} onChange={(e) => onChange(e)} placeholder="Soyad" className="panelprflinput"></input>
                                <input type="textarea" name="username" value={username} onChange={(e) => onChange(e)} placeholder="Restoran Adi" className="panelprflinput"></input>
                                <p onClick={showpass} className="changepass">Şifrəni dəyiş</p>
                                
                            </div>
                            <button className="saveallchpop" type="submit" onClick={closem} >Yadda saxla</button>

                        </div>
                        </form>
                    </div>
                    <UpdatePass/>
                </div>
             </div>
        );
    }

    export default connect(null, { profile_update })(ProfileUpdate);