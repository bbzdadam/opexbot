import React from 'react'
import ModalKuriyer from './ModalKuriyer'
class Kuriyer extends React.Component {
    state = {
        showModal: 0
    };
    
      getModal = value => {
        this.setState({ showModal: value });
    };
    
      hideModal = value => {
        this.setState({ showModal: 0 });
    };

    



    render() {

        if(!this.props) return 'Loading';
        
        return (
            <div>
                  {this.props.courier.map((mdata, key)=> (
                    <div className="kuryerlist"> 
                        <div className="kuriyer">
                            <p className="esasdetal">{mdata.name} {mdata.surname}</p>
                            <p className="esasdetalnom">{mdata.phone_number}</p>
                            <p className="kuriyerstatus">Active</p>
                            <img onClick={() => this.getModal(mdata.id)} className="kuriyerduzelish" src="https://res.cloudinary.com/dgslkvls9/image/upload/v1600434247/Pen_Edit.8_1_akastz.png"></img>
                        </div>

                        <ModalKuriyer
                            show={this.state.showModal === mdata.id}
                            onHide={() => this.hideModal(mdata.id)}
                            name={mdata.name} 
                            surname={mdata.surname}
                            phnumber={mdata.phone_number}
                            status={mdata.status}
                            id={mdata.id}
                        />  
                    </div>
                ))}  
            </div>
        );
    }
}

export default Kuriyer