import React from 'react'
import SidebarStat from '../Main/SidebarStat'
import HeaderStat from '../Main/HeaderStat'
import StatBoard from './StatBoard'
import StatChart from './StatChart'


const Statistics = () => {

    return (
        <div className="adtapmadim">
            <SidebarStat />
            <HeaderStat />
            <div className="statbody">
                <StatBoard/>
                <StatChart/>
            </div>  

      
    </div>
    );
}

export default Statistics