import React from 'react'
import Chart from './Chart'
import ColumnChart from './ColumnChart'
import DropdownButton from 'react-bootstrap/DropdownButton'
import Dropdown from 'react-bootstrap/Dropdown'




class StatChart extends React.Component {
    render(){
        
        return(
            <div className="chartbody">
                <div className="statChart">
                    <div className="showtype">
                        <div className="droprange">
                            <DropdownButton id="dropdown-basic-button" title="Annual" src="https://res.cloudinary.com/dgslkvls9/image/upload/v1599824897/expand-more-material_3x_nxwa4w.png" >
                                <div className="statdrop">
                                <Dropdown.Item id="dropdowncontent" href="#/action-1">Monthly</Dropdown.Item>
                                <Dropdown.Item id="dropdowncontent" href="#/action-2">Weekly</Dropdown.Item>
                                <Dropdown.Item id="dropdowncontent" href="#/action-3">Daily</Dropdown.Item>
                                </div>
                            </DropdownButton>
                        </div>
                        <p className="ordercounttype">Total orders</p>
                        <p className="totalcount">19,232</p>
                        <a className="updownrate"><img className="updown" src="https://res.cloudinary.com/dgslkvls9/image/upload/v1599565617/call-made-material_3x_vzxkcj.png" ></img><p className="rateer">+33.45%</p></a>
                    </div>

                    <div className="columnchart">
                        <ColumnChart/>
                    </div>

                    <div className="poporderlist">
                        <div className="poporders">
                            <p className="popularord">Popular orders</p>
                            <a className="seeall"><p >See all</p></a>  
                        </div>  
                        <div className="toporders">
                            <div className="toporderdetails">
                                <p className="orderplace">1</p>
                                <p className="orderName">Cheesburger Combo</p>
                                <img className="popularup" src="https://res.cloudinary.com/dgslkvls9/image/upload/v1599565617/call-made-material_3x_vzxkcj.png"></img>
                                <p className="increaserate">+33,45%</p>
                                <p className="topordercount">x534</p>
                                
                            </div>
                            <hr className="downside" ></hr>
                            <div className="toporderdetails">
                                <p className="orderplace">2</p>
                                <p className="orderName">Cheesburger Combo</p>
                                <img className="popularup" src="https://res.cloudinary.com/dgslkvls9/image/upload/v1599565617/call-made-material_3x_vzxkcj.png"></img>
                                <p className="increaserate">+33,45%</p>
                                <p className="topordercount">x534</p>
                                
                            </div>
                            <hr className="downside" ></hr>
                            <div className="toporderdetails">
                                <p className="orderplace">3</p>
                                <p className="orderName">Cheesburger Combo</p>
                                <img className="popularup" src="https://res.cloudinary.com/dgslkvls9/image/upload/v1599565617/call-made-material_3x_vzxkcj.png"></img>
                                <p className="increaserate">+33,45%</p>
                                <p className="topordercount">x534</p>
                                
                            </div>

                    
                        </div> 
                
                    </div> 

                </div>
                <div className="statrightch">
                    <div className="roundchart">
                        <div className="boshbishey">

                        </div>
                        <div className="rrchrt">
                            <Chart/>
                    </div>
                    </div>

                    <div className="totalearning">
                        <div className="earningdetails">
                            <p className="totalearned">Total earnings</p>
                            <p className="balance">$20,906</p>
                            <p className="yoxlagorm"><img className="balancestat" src="https://res.cloudinary.com/dgslkvls9/image/upload/v1599565521/call-made-material_3x_xsqujk.png" ></img></p>
                            <p className="balancerate"> +13.45% </p>
                            <p className="learnbalance"> Learn more</p>
                        </div>
                    </div>
                </div>

            </div>    
        )
    }
}

export default StatChart