import React from 'react'
import ApexCharts from 'apexcharts'
import ReactApexChart from 'react-apexcharts'

class ApexChart extends React.Component {
    constructor(props) {
      super(props);

      this.state = {
      
        series: [ {
          name: 'Total Cash Flow',
          data: [35, 41, 36, 26, 45, 48, 52, 53, 41, 20, 30, 60 ],
          colors: ['#03c56d']
        }],
        options: {
          chart: {
            type: 'bar',
            height: 350,
            toolbar: {
                show: false
            },
          },
          plotOptions: {
            bar: {
              horizontal: false,
              columnWidth: '25%',
            },
          },
          dataLabels: {
            enabled: false
          },
          stroke: {
            show: true,
            width: 2,
            colors: ['#03c56d']
          },
          xaxis: {
            categories: ['Jan','Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'] || ['#8080a4'],
            labels: {
                style: {
                  colors: '#8080a4',
                  fontSize: '16px',
                  fontWeight: 'bold',
                  fontFamily: "Inter"
                }
              }
          },

          yaxis: {
              labels: {
                  style : {
                      colors: '#8080a4',
                      fontSize: '16px',
                      fontWeight: 'bold',
                      fontFamily: "Inter"
                  }
              }
          },
          fill: {
            opacity: 1,
            colors: ['#03c56d']
          },
          tooltip: {
            y: {
              formatter: function (val) {
                return "$ " + val + ""
              }
            }
          }
        },
      
      
      };
    }

  

    render() {
      return (
        

  <div id="chart">
        <ReactApexChart options={this.state.options} series={this.state.series} type="bar" height={300} />
</div>


      );
    }
  }

  export default ApexChart