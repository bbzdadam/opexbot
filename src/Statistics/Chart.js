import React from 'react';
import ApexCharts from 'apexcharts'
import ReactApexChart from 'react-apexcharts';

class ApexChart extends React.Component {
    constructor(props) {
      super(props);

      this.state = {
      
        series:  [75, 25],   
        
        options: {
          labels: ["Delivery", "Pickup"],
          colors: ['#03c56d', '#ffb800'],
          legend : {
            offsetY: 6,
            height: '60px',
            position: "bottom",
            fontSize: '14px',
            fontFamily: 'Inter',
            fontWeight: "bold",
                        
          },

          dataLabels : {
            style: {
              show: 'none',
              width: '56px',
              height: '18px',
              fontSize: '0px',
              colors: ['#000048'],
              fontWeight: 500,
              fontFamily: 'Inter',
              },
          },
          chart: {
            type: 'donut',
            position: 'center'
          },
          plotOptions: {
            pie: {
              donut: {
                size: '86%',
              },
              offsetX: -5,
              offsetY: 20,
              customScale: 1.2,
            },
          },

        },     
      
      };
    }

  

    render() {
      return (
        

  <div id="chart">
<ReactApexChart options={this.state.options} series={this.state.series} type="donut" />
</div>


      );
    }
  }

  export default ApexChart