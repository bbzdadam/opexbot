import React from 'react' 

class StatBoard extends React.Component {
    render(){
        return (
            <div className="statboard">
                <div className="board">                   
                    <p className="ordercounttype">Total orders</p>
                    <p className="totalcount">19,232</p>
                    <a className="updownrate">
                        <img className="updown" src="https://res.cloudinary.com/dgslkvls9/image/upload/v1599565617/call-made-material_3x_vzxkcj.png" ></img>
                         <p className="rateer">+33.45%</p>
                    </a>
                </div>

                <div className="board">
                    <p className="ordercounttype">Monthly</p>
                    <p className="totalcount">4,345</p>
                    <a className="updownrate">
                        <img className="updown" src="https://res.cloudinary.com/dgslkvls9/image/upload/v1599565617/call-made-material_3x_vzxkcj.png" ></img>
                        <p className="rateer">+33.45%</p>
                    </a>

                </div>
                <div className="board">
                    <p className="ordercounttype">Weekly</p>
                    <p className="totalcount">1202</p>
                    <a className="updownrate">
                        <img className="updown" src="https://res.cloudinary.com/dgslkvls9/image/upload/v1599565617/call-made-material_3x_vzxkcj.png" ></img>
                        <p className="rateer">+33.45%</p>
                    </a>

                </div>
                <div className="board">
                    <p className="ordercounttype">Daily</p>
                    <p className="totalcount">102</p>
                    <a className="updownrate">
                        <img className="updown" src="https://res.cloudinary.com/dgslkvls9/image/upload/v1599565617/call-made-material_3x_vzxkcj.png" ></img>
                        <p className="rateer">+33.45%</p>
                    </a>
                </div>
                
            </div>
        )
    }
    
}

export default StatBoard    