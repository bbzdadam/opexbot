import React, { Component } from "react";
import InOrders from "./InOrders";
import Timer from './Timer';

function next() {
	var x = document.getElementById("popupwind");
	var y = document.getElementById("pickinup");
    if (x.style.display === "block" && y.style.display === "none") {
	  x.style.display = "none";
	  y.style.display = "block";
    } else {
	  x.style.display = "none";
	  y.style.display = "block";
    }
}

function changeest() {
	var x = document.getElementById("miniord");
	var y = document.getElementById("timeslot");
    if (x.style.display === "block" && y.style.display === "none") {
		x.style.display = "none";
		y.style.display = "block";
	} else {
		x.style.display = "none";
		y.style.display = "block";
	}
}


function cancel() {
	var x = document.getElementById("timeslot");
	var y = document.getElementById("miniord");
    if (x.style.display === "block" && y.style.display === "none") {
		x.style.display = "none";
		y.style.display = "block";
	} else {
		x.style.display = "none";
		y.style.display = "block";
	}
    
}

function showMore() {
	var x = document.getElementById("nomore");
	var y = document.getElementById("moreless");
    if (x.style.display === "block" && y.style.display === "none") {
	  x.style.display = "none";
	  y.style.display = "block";
    } else {
	  x.style.display = "none";
	  y.style.display = "block";
    }
}

function showLess() {
	var x = document.getElementById("moreless");
	var y = document.getElementById("nomore");
    if (x.style.display === "block" && y.style.display === "none") {
	  x.style.display = "none";
	  y.style.display = "block";
    } else {
	  x.style.display = "none";
	  y.style.display = "block";
    }
}

function goBack(){
	var x = document.getElementById("pickinup");
	var y = document.getElementById("popupwind");
    if (x.style.display === "block" && y.style.display === "none") {
	  x.style.display = "none";
	  y.style.display = "block";
    } else {
	  x.style.display = "none";
	  y.style.display = "block";
    }

}
/*
function buton(){
	
	let btnTime = document.querySelector('#timebutton');
	let applyBtn = document.querySelector('#applybtn');

	btnTime.addEventListener('click', ()=>applyBtn.style.backgroundColor='#03c56d');
}
*/


class Modalout extends Component {

	constructor(){
		super()
		this.state = {
			delivery: true	
		}
	}


  render() {
	if(!this.props.mehsullar) return 'Loading';
	let orderType

	if(this.state.delivery === true){
		orderType = "https://res.cloudinary.com/dgslkvls9/image/upload/v1598770300/delivery_3x_bbxxjy.png"
	} else {
		orderType = "https://res.cloudinary.com/dgslkvls9/image/upload/v1598770299/cars_3x_k9akn6.png"
	}

    console.log(this.props.show);
    return (
      <React.Fragment>
        {this.props.show && (
		
          <div className="modalout">
				<div className="btnx">
			  		<button className="closebtes" onClick={this.props.onHide}><img className="clsbtnmg" src="https://res.cloudinary.com/dgslkvls9/image/upload/v1600776523/Add_Cirlce_Plus_4_3x_s4bc4m.png"></img></button>
				</div>
				<div class="popuprectout">

					<div id="popupwind" class="detallar">
						<div className="detallar_credent">
								<div className="credentials_first">
									<div className="credentials_first_head">
										<p class="idnumber">#{this.props.daily_id}</p>
										<p class="musteridetal">{this.props.name} {this.props.surname}</p>
									</div>
									<p class="phnumber">{this.props.phone_number}</p>		
									<p className="unvani"> {this.props.location} </p>
									<div className="dialogbox">
										<div class="comment ">
											<p class="cmt">{this.props.comment} </p>
										</div>
									</div> 

								</div>
								<div className="credentials_second">
									<Timer/>
									<p className="deadline_time"> --:--</p>
									<img  class="order_type_delivery" src={orderType}></img>
									<p class="order_status">{this.props.types}</p>
								</div>
							</div>
							<hr />
						<div className="sifarisler">					
							<InOrders order={this.props.mehsullar}/>						
							
						</div>
						
						<div className="totalprice_buttonout">					
                            <button className="readyfordelivery">Ready for delivery</button>
						</div>
					</div>
				</div>  
          </div>
        )}
      </React.Fragment>
    );
  }
}

export default Modalout;
