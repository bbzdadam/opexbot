import React from 'react'
import outgoingOrderData from './outgoingOrderData'
import Timer from './Timer'

class OutOrders extends React.Component {
	render() {
		return (
			<div class="mainrect">
				<div class="details">
					<div class="userdetails">
						<p class="idnumber">#{this.props.nid}</p>
						<p class="namsesurname">{this.props.fullname}</p>
						<div id="timerapp">
							<Timer/>
							<p className="deadline_time">14:00</p>	
						</div>
					</div>
					
					<div class="telnumber">
						<p class="phnumber">{this.props.phnumber}</p>
					</div>					
					<hr />
					<img  class="order_type_delivery" src="https://res.cloudinary.com/dgslkvls9/image/upload/v1598770300/delivery_3x_bbxxjy.png"></img>
					<img className="order_type_pickup" src="https://res.cloudinary.com/dgslkvls9/image/upload/v1598770299/cars_3x_k9akn6.png"></img>
                    <p class="order_status">{this.props.type}</p>
                    
					<div class="orderdetails_1">
						<p class="count">{this.props.count}x</p>
						<p class="foodname">{this.props.food}</p>
					</div>
					<div class="dtl">	
						<p class="fooddetails"> {this.props.fooddetails}</p>
					</div>
					<hr />					
					
					<div class="orderdetails_2">
						<p class="countes">{this.props.countes}x</p>
						<p class="foodname">{this.props.drink}</p>
					</div>	
					<hr />
					<div class="price">
						<p class="total">{this.props.totalprice} AZN</p>
					</div>	
						
					<div class="btns">
						<button id="switch" class="next"> Ready for delivery </button>
					</div>
					
				</div>
				
			</div> 
		);
	}
}

export default OutOrders