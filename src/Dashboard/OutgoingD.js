import React from 'react'
import Timer from './Timer';
import Modalout from './ModalOut';

function showhide(){      
    var div = document.getElementById("a");  
    var z = document.getElementById("b"); 
    if (div.style.display !== "none" && z.style.display === "none") {  
        div.style.display = "none"; 
        z.style.display = "block"   
    }else{  
        div.style.display = "block";
        z.style.display = "none";  
    }  
}

function hideshow(){      
    var div = document.getElementById("b");  
    var z = document.getElementById("a"); 
    if (div.style.display !== "none" && z.style.display === "none") {  
        div.style.display = "none"; 
        z.style.display = "block" 
    }else{  
        div.style.display = "block";
        z.style.display = "none";  
    }  
} 




class OutgoingD extends React.Component {

    constructor(){
		super()
		this.state = {
			delivery: true	
		}
	}

    state = {
        showModal: 0
    };
    
      getModal = value => {
        this.setState({ showModal: value });
    };
    
      hideModal = value => {
        this.setState({ showModal: 0 });
    };





    render(){

        let orderType

		if(this.state.delivery === false){
			orderType = "https://res.cloudinary.com/dgslkvls9/image/upload/v1598770300/delivery_3x_bbxxjy.png"
		} else {
			orderType = "https://res.cloudinary.com/dgslkvls9/image/upload/v1598770299/cars_3x_k9akn6.png"
		}
        
        return (

            <div className="mainrect">
                {this.props.outgoing.map((data, key)=> (
                    
                    <div id="truetone" className="medium">
                        <div className="cardleft" onClick={() => this.getModal(data.id)}>
                            <div className="detailsforid">
                                <p className="orderidnum"> #{data.daily_id} </p>
                                <img className="orderdeltyp" src={orderType}></img>
                                <img className="paymenttype" src="https://res.cloudinary.com/dgslkvls9/image/upload/v1605304751/Credit_card_3x_bg1btb.png" ></img>

                            </div>

                            <div  className="sifariscidetal">
                                <div className="part1">
                                    <p className="sifarisci">{data.client.name} {data.client.surname}</p>
                                    <p className="nomresi">{data.client.phone_number}</p>
                                    <p className="unvan">{data.location}</p>
                                </div>
                            </div>

                            <div  className="mehsulsayi">
                                <p className="say">{data.items.length} növ məhsul</p>
                            </div>
                        </div>

                        <div className="cardright">
                            <div className="part2">
                               <Timer timesc={data.timesc}/>
                                <p className="vaxt"> --:-- </p>
                            </div>
                            <p className="yekunmebleg">{data.total} ₼</p>
                        </div>
                       
                        <Modalout
                        show={this.state.showModal === data.id}
                        onHide={() => this.hideModal(data.id)}
                        daily_id={data.daily_id}
                        name={data.client.name}
                        surname={data.client.surname}
                        location={data.client.location}
                        phone_number={data.client.phone_number}
                        comment={data.comment}
                        timesc={data.timesc}
                        dedtime={data.deadline}
                        total={data.total}
                        deliverytype={data.deliverytype}
                        mehsullar={data.items}
                        />
                    </div>
                ))}

                <div id="b" onClick={hideshow} className="hideme">
                    <div className="outbtns">
                        <button className="rdyfordeli">Ready for delivery </button>
                        <button className="moredetails">Details</button>
                    </div>
                </div>

            </div>
        )
    }
}

export default OutgoingD