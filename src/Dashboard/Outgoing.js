import React from 'react'
import OutgoingD from './OutgoingD'
import { outgoing_list } from '../actions/auth'
import { connect } from 'react-redux';
/*
const outorderComponents = outgoingOrderData.map(
    order =><OutgoingD key={order.id}
    nid = {order.nid}
    fullname = {order.fullname}
    phnumber = {order.phnumber}
    type = {order.type}
    count = {order.count}
    food = {order.food}
    fooddetails = {order.fooddetails}
    countes = {order.countes}
    drink = {order.drink}
    location={order.location}
    totalprice  = {order.totalprice} />)
*/



class Outgoing extends React.Component {

      constructor(props) {
        super();
        props.outgoing_list();        
    }

    

    render() {
        
        let outgoingdata = this.props.outgoing;
        if (!outgoingdata) return 'Loading';
        console.log(this.props)
        

        return (
            <div class="outgoing">
			    <h5>Outgoing ({outgoingdata.length})</h5>
                <div className="incominglist">
			       <OutgoingD outgoing={this.props.outgoing}/> 
                </div>
		    </div>
        )
    }   
}

export default connect(state => ({outgoing: state.auth.outgoing}), {outgoing_list})(Outgoing)
