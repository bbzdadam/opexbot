import React from 'react'

class InOrders extends React.Component {
    render() {
        // if (!this.props.orddata) return 'Loading';

        return (
            <div className="inordmap">
                {this.props.order.map((orddata, key)=> (
                <div>
                    <div class="orderdetails_1">
                        <p class="count">{orddata.quantity}x</p>
                        <p class="foodname">{orddata.product.name}</p>
                    </div>
                    <div class="dtl">	
                        <p class="fooddetails"> {orddata.product.ingridients}</p>
                    </div>
                    	
                    <hr />
                </div>
                ))}
            </div>
        );
    }
}

export default InOrders