import React from 'react'



function schedule(){
    const popup = document.querySelector('.change_estimate');
    popup.classList.add('open');
    
    const accpopup = document.querySelector('.acceptance');
    accpopup.classList.add('closed')
        
}

function cancsch() {
    const popup = document.querySelector('.change_estimate');
    popup.classList.remove('open');
    const accpopup = document.querySelector('.acceptance');
    accpopup.classList.remove('closed');

}

function cancel() {
    var x = document.getElementById("popupwindow");
    if (x.style.display === "block") {
      x.style.display = "none";
    } else {
      x.style.display = "none";
    }
}

class Popup extends React.Component {
	render() {
		return (
		<div className="backgrclr">
			<div className="popupwindow" id="popupwindow">
				<div class="lighboxpop" id="lighboxpop">
					<div class="orderdetails_mini">
						<div class="details">
							<div class="userdetails">
								<p class="idnumber">#  {this.props.nid} </p>
								<p class="namsesurname">{this.props.fullname}</p>
							</div>
											
							<div class="telnumber">
								<p class="phnumber">{this.props.phnumber}</p>
							</div>
						
							<div class="mainstatus">
								<img  class="order_type_pickup"></img>
								<p class="order_status">{this.props.type}</p>
							</div>
							<hr/>
						
							<div class="orderdetails_1">
								<p class="count">{this.props.count}x</p>
								<p class="foodname">{this.props.food}</p>
							</div>
						
							<div class="dtl">	
								<p class="fooddetails"> {this.props.fooddetails}</p>
							</div>
						
											
							<hr/>
							<div class="orderdetails_2">
								<p class="countes">{this.props.countes}x</p>
								<p class="foodname">{this.props.drink}</p>
							</div>	
							<hr/>
							<div class="price">
								<p class="total">{this.props.totalprice} AZN</p>
							</div>	
						
						</div>
			
					</div>
			
					<div id="accept" class="acceptance">
						<h3>Pickup in 25 min.Can you make it?</h3>
						<p>This will be sent to the customer</p>
				
						<div class="popbtns">
							<button class="accept"> Yes, let’s do it! </button>
							<button onClick={schedule} class="change"> No, change estimate </button>
							<button onClick={cancel} class="cancel"> Cancel </button>
						</div>
				
					</div>
	
					<div id="schedule" class="change_estimate">
						<div class="timeschedule">
							<button class="choosemin" >5   </button>
							<button class="choosemin" >10  </button>
							<button class="choosemin" >15  </button>
							<button class="choosemin" >20  </button>
							<button class="choosemin" >25  </button>
							<button class="choosemin" >30  </button>
							<button class="choosemin" >35  </button>
							<button class="choosemin" >40  </button>
							<button class="choosemin" >45  </button>
							<button class="choosemin" >50  </button>
							<button class="choosemin" >55  </button>
							<button class="choosemin" ><input placeholder="Custom" type="text" className="changetime"></input>   </button>
						</div>
	
						<div class="popbtns">
							<button onClick={cancsch} class="cancel"> Cancel </button>
						</div>
					</div>
				</div>		
			</div>
		</div>
			
	
	)

}
}
export default Popup