
import React from 'react'
import readyOrderData from './readyOrderData'
import {ready_list} from '../actions/auth'
import {connect} from 'react-redux'
import ReadyOrderComps from './ReadyOrderComps'

class Ready extends React.Component {
    constructor(props) {
        super();
        props.ready_list();        
    }

    

    render() {
        return (
            <div class="ready">
			    <h5>Ready ({readyOrderData.length})</h5>
			    <ReadyOrderComps/>
            </div>
        )
    }  
}

export default connect(null, {ready_list})(Ready)
