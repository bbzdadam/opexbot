import React from 'react'
import Timer from './Timer'

/*function lighbox() {
    var x = document.getElementById("popupwindow");
    if (x.style.display === "block") {
      x.style.display = "none";
    } else {
      x.style.display = "block";
    }
} */

class Orders extends React.Component {

	constructor(){
		super()
		this.state = {
			delivery: true	
		}
	}

	render() {

		let orderType

		if(this.state.delivery === true){
			orderType = "https://res.cloudinary.com/dgslkvls9/image/upload/v1598770300/delivery_3x_bbxxjy.png"
		} else {
			orderType = "https://res.cloudinary.com/dgslkvls9/image/upload/v1598770299/cars_3x_k9akn6.png"
		}

		return (
		<div className="incominglist">
			<div class="mainrect">
        		<div class="details">
					<div class="userdetails">
						<p class="idnumber">#{this.props.nid}</p>
						<p class="namsesurname">{this.props.fullname}</p>
						<div id="timerapp">
							<div className="ttmr">
								<Timer/>
							</div>
						<p className="deadline_time">14:00</p>
						</div>	
					</div>
			
					
					<div class="telnumber">
						<p class="phnumber">{this.props.phnumber}</p>
					</div>


					<div class="mainstatus">
						<img  class="order_type_delivery" src={orderType}></img>
						<p class="order_status">{this.props.type}</p>
					</div>


					<div className="dialogbox">
						<div class="comment ">
							<p class="cmt">{this.props.comment} </p>
						</div>
					</div> 
					<hr />
					
					<div class="orderdetails_1">
						<p class="count">{this.props.count}x</p>
						<p class="foodname">{this.props.food}</p>
					</div>
					
					<div class="dtl">	
						<p class="fooddetails"> {this.props.fooddetails}</p>
					</div>
					<hr />		
					<div class="orderdetails_2">
						<p class="countes">{this.props.countes}x</p>
						<p class="foodname">{this.props.drink}</p>
					</div>	
					<hr />	
					
							
					<div class="price">
						<p class="total">{this.props.totalprice	} AZN</p>
					</div>	
						
					<div class="btns">
						<button onClick class="next"> Next </button>
						<button class="reject"> Reject </button>
					</div>
					
				</div>
			</div>
		</div>
		)
	}
}

	

export default Orders