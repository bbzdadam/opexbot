import React from 'react'
import Sidebar from '../Main/Sidebar'
import Header from '../Main/Header'
import Incoming from './Incoming'
import Outgoing from './Outgoing'
import Ready from './Ready'
import Popup from './Popup'
import Modal from './Modal'




const Dashboard = () => {

    return (
        <div className="adtapmadim">
            <Sidebar />
            <Header />
            <div className="mainbody">
                <Incoming />
                <Outgoing />
                <Ready /> 
            </div>  
            
    </div>
    );
}

export default Dashboard