import React from 'react'
import Timer from './Timer'
import Modal from './Modal'

class IncomingD extends React.Component {

    constructor(){
		super()
        
	}

    state = {
        showModal: 0
    };
    
      getModal = value => {
        this.setState({ showModal: value });
    };
    
      hideModal = value => {
        this.setState({ showModal: 0 });
    };



    render(){

        return (
            
            <div  className="mainrect">
                {this.props.incoming.map((data, key)=> (
                    
                    <div id="truetone" className="small">
                        <div className="cardleft" onClick={() => this.getModal(data.id)}>
                            <div className="detailsforid">
                                <p className="orderidnum"> #{data.daily_id} </p>
                                <img className="orderdeltyp" src={data.deliveryType === 1 ? 'https://res.cloudinary.com/dgslkvls9/image/upload/v1598770300/delivery_3x_bbxxjy.png' : data.deliveryType === 2 ? 'https://res.cloudinary.com/dgslkvls9/image/upload/v1598770299/cars_3x_k9akn6.png' : ''}></img>
                                <img className="paymenttype" src="https://res.cloudinary.com/dgslkvls9/image/upload/v1605304751/Credit_card_3x_bg1btb.png" ></img>
                            </div>
            

                            <div  className="sifariscidetal">
                                <div className="part1">
                                    <p className="sifarisci">{data.client.name} {data.client.surname} </p>
                                    <p className="nomresi">{data.client.phone_number}</p>
                                    <p className="unvan">Ataturk pr 89</p>
                                </div>
                            </div>

                            <div  className="mehsulsayi">
                                <p className="say">{data.items.length} növ məhsul</p>
                            </div>
                        </div>

                        <div className="cardright" onClick={() => this.getModal(data.id)}>
                            <div className="part2">
                               <Timer timesc={data.timesc} />
                                <p className="vaxt"> --:-- </p>
                            </div>
                            <p className="yekunmebleg">{data.total} ₼</p>
                        </div>
                        <Modal
                        show={this.state.showModal === data.id}
                        onHide={() => this.hideModal(data.id)}
                        daily_id={data.daily_id}
                        name={data.client.name}
                        surname={data.client.surname}
                        location={data.client.location}
                        phone_number={data.client.phone_number}
                        comment={data.comment}
                        timesc={data.timesc}
                        dedtime={data.deadline}
                        total={data.total}
                        deliverytype={data.deliveryType}
                        mehsullar={data.items}
                        status={data.status}
                        orderID={data.id}
                        

                        />
                    </div>

                ))}
            

            </div>
            
            
        )
    }
}

export default IncomingD