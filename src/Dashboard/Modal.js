import React, { Component } from "react";
import InOrders from "./InOrders";
import Timer from './Timer';
import {incomingto_out} from '../actions/auth'
import {connect} from 'react-redux'
import CourierDrop from "./CourierDrop";

function next() {
	var x = document.getElementById("popupwind");
	var y = document.getElementById("pickinup");
    if (x.style.display === "block" && y.style.display === "none") {
	  x.style.display = "none";
	  y.style.display = "block";
    } else {
	  x.style.display = "none";
	  y.style.display = "block";
    }
}

function changeest() {
	var x = document.getElementById("miniord");
	var y = document.getElementById("timeslot");
    if (x.style.display === "block" && y.style.display === "none") {
		x.style.display = "none";
		y.style.display = "block";
	} else {
		x.style.display = "none";
		y.style.display = "block";
	}
}


function cancel() {
	var x = document.getElementById("timeslot");
	var y = document.getElementById("miniord");
    if (x.style.display === "block" && y.style.display === "none") {
		x.style.display = "none";
		y.style.display = "block";
	} else {
		x.style.display = "none";
		y.style.display = "block";
	}
    
}

function showMore() {
	var x = document.getElementById("nomore");
	var y = document.getElementById("moreless");
    if (x.style.display === "block" && y.style.display === "none") {
	  x.style.display = "none";
	  y.style.display = "block";
    } else {
	  x.style.display = "none";
	  y.style.display = "block";
    }
}

function showLess() {
	var x = document.getElementById("moreless");
	var y = document.getElementById("nomore");
    if (x.style.display === "block" && y.style.display === "none") {
	  x.style.display = "none";
	  y.style.display = "block";
    } else {
	  x.style.display = "none";
	  y.style.display = "block";
    }
}

function goBack(){
	var x = document.getElementById("pickinup");
	var y = document.getElementById("popupwind");
    if (x.style.display === "block" && y.style.display === "none") {
	  x.style.display = "none";
	  y.style.display = "block";
    } else {
	  x.style.display = "none";
	  y.style.display = "block";
    }

}

/*
function buton(){
	
	let btnTime = document.querySelector('#timebutton');
	let applyBtn = document.querySelector('#applybtn');

	btnTime.addEventListener('click', ()=>applyBtn.style.backgroundColor='#03c56d');
}
*/


class Modal extends Component {

	constructor(){
		super()
	}
	

	onSubmit = (id) => {
		id.preventDefault();
		this.props.incomingto_out(id);
	}

	

  render() {
	  
	if(!this.props.mehsullar) return 'Loading';
	console.log(this.props.orderID)

	


    return (


      <React.Fragment>
		  
        {this.props.show &&  (
		
          <div className="modal">
				<div className="btnx">
			  		<button className="closebt" onClick={this.props.onHide}><img className="clsbtnmg" src="https://res.cloudinary.com/dgslkvls9/image/upload/v1600776523/Add_Cirlce_Plus_4_3x_s4bc4m.png"></img></button>
				</div>
				<div class="popuprect">
					
					<div id="popupwind" class="detallar">

						<div className="detallar_credent">
							<div className="credentials_first">
								<div className="credentials_first_head">
									<p class="idnumber">#{this.props.daily_id}</p>
									<p class="musteridetal">{this.props.name} {this.props.surname}</p>
								</div>
								<p class="phnumber">{this.props.phone_number}</p>		
								<p className="unvani"> Ataturk pr 89  </p>
								<div className="dialogbox">
									<div class="comment ">
										<p class="cmt">{this.props.comment} </p>
									</div>
								</div> 

							</div>
							<div className="credentials_second">
								<Timer/>
								<p className="deadline_time"> --:-- </p>
								<img  class="order_type_delivery" src={this.props.deliverytype === 1 ? 'https://res.cloudinary.com/dgslkvls9/image/upload/v1598770300/delivery_3x_bbxxjy.png' : this.props.deliverytype === 2 ? 'https://res.cloudinary.com/dgslkvls9/image/upload/v1598770299/cars_3x_k9akn6.png' : ''}></img>
								<p class="order_status">{this.props.types}</p>
							</div>
						</div>

						<hr />
						<div className="sifarisler">
							<InOrders order={this.props.mehsullar} />
						</div>
						
						<div className="totalprice_button">					
							<div class="price">
								<p class="total">{this.props.total	} AZN</p>
							</div>

							<div class="btns">
								<button class="reject"> Reject </button>
								<button onClick={next} class="next"> Next </button>		
							</div>	
						</div>
					</div>

					<div id="pickinup" className="pickupphase">
						<button className="gobackbt" onClick={goBack}><img className="clsbtnmg" src="https://res.cloudinary.com/dgslkvls9/image/upload/v1601031582/Arrow_Right_m0nuzu.png"></img></button>

						<div className="pickup_text">
							<h6 className="questionary"> Pickup in 30 min. <br></br> Can you make it? </h6>
							<p className="informative">This will be sent to the customer</p>
						</div>

						<CourierDrop/>


						<div id="miniord" className="miniorderdet">

							<div id="moreless" className="borderboxorig">
								<div className="usernn">
									<p class="useridnum">#{this.props.daily_id}</p>
									<p class="useriddet">{this.props.name} {this.props.surname}</p>
								</div>
								<div className="userunvan">
									<div className="userunvminid">
										<p className="usernom"> {this.props.phone_number} </p>
										<p className="userunv"> {this.props.location} </p>
									</div>
									<div className="usermeb">
										<p className="usermebleg"> {this.props.total} AZN </p>
									</div>
								</div>
								<hr className="hrmini"/>
								
								<div className="orderscroll">
									<InOrders order={this.props.mehsullar}/>
								</div>

								<img onClick={showLess}  className="showless" src="https://res.cloudinary.com/dgslkvls9/image/upload/v1601029542/Arrow.2_1_nwmdoz.png"></img>
							</div>
							
								<div id="nomore" className="borderboxmini">
										<div className="usernn">
											<p class="useridnum">#{this.props.daily_id}</p>
											<p class="useriddet">{this.props.name} {this.props.surname}</p>
										</div>
										<div className="userunvan">
											<div className="userunvminid">
												<p className="usernom"> {this.props.phone_number} </p>
												<p className="userunv"> {this.props.location} </p>
											</div>
											<div className="usermeb">
												<p className="usermebleg"> {this.props.total} AZN </p>
											</div>
										</div>
										<hr className="hrmini"/>

										<div className="orderscrollmini">
											<InOrders order={this.props.mehsullar}/>
										</div>

									<img onClick={showMore} className="dropseemore" src="https://res.cloudinary.com/dgslkvls9/image/upload/v1601030057/expand-more-material_ku5dd4.png"></img>
								</div> 
							
							<div class="btnes">
								<button onClick={changeest} class="change"> No, change estimate </button>
								<button type="submit" class="accept" onClick={(id)=>this.onSubmit(this.props.id, id)}> Yes, let’s do it! </button>		
							</div>
						</div>

						<div id="timeslot" className="vaxtaraligi">	
							<div class="change_estimate">
								<div class="timeschedule">
									<button    id="timebutton" class="choosemin" >5   </button>
									<button    id="timebutton" class="choosemin" >10  </button>
									<button    id="timebutton" class="choosemin" >15  </button>
									<button    id="timebutton" class="choosemin" >20  </button>
									<button    id="timebutton" class="choosemin" >25  </button>
									<button    id="timebutton" class="choosemin" >30  </button>
									<button    id="timebutton" class="choosemin" >35  </button>
									<button    id="timebutton" class="choosemin" >40  </button>
									<button    id="timebutton" class="choosemines" >45  </button>
									<button    id="timebutton" class="choosemines" >50  </button>
									<button    id="timebutton" class="choosemines" >55  </button>
									<button    id="timebutton" class="choosemin" ><input placeholder="Custom" type="text" className="changetime"></input>   </button>
								</div>
							</div>	


							<div className="xatirlatici">
								<div className="nomreuser">
									<p class="idnumber">#{this.props.daily_id}</p>
									<p class="musteridetal">{this.props.name} {this.props.surname}</p>
								</div>
								<div className="nomunvanmeb">
									<div className="nomunvan">
										<p className="mininom"> {this.props.phone_number} </p>
										<p className="miniunv"> Ataturk pr 89 </p>
									</div>
									<div className="meb">
										<p className="minimebleg"> {this.props.total} AZN </p>
									</div>
								</div>

								<div className="btns">
									<button onClick={cancel} class="cancel"> Cancel </button>
									<button id="applybtn" class="apply"> Apply </button>
								</div>
							</div>
						</div>
	

					</div>
				</div>  
          	</div>
        )}
    	</React.Fragment>
    );
  }
}

export default connect(null, { incomingto_out })(Modal);