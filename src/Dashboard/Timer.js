import React from "react";
import { CountdownCircleTimer } from "react-countdown-circle-timer";
import Incoming from './Incoming'

const renderTime = ({ remainingTime }) => {
	return (
	  <div className="timer">
		<div className="value">{(remainingTime /60).toFixed(0)}</div>
	  </div>
	);
  };
 


function App (timesc) {

	const key = timesc;
		
	return (
		<React.Fragment>
			
			<div className="App">
				<div className="timer-wrapper">
							
					<CountdownCircleTimer
					key = {key}				
					size="60"
					strokeWidth= "3"
					rotation= "counterclockwise"
					isPlaying
					duration={ 30 * 60}
					colors={[ ['#03c56d', 0.55],
							['#FAE52C', 0.25],
							['#FF9921', 0.10],
							['#A30000', 0.10] ]}
					onComplete={() => [false]}
					>
					{renderTime}
					</CountdownCircleTimer>
				</div>
			</div>
		</React.Fragment>	
	);
	
  }

  export default App
