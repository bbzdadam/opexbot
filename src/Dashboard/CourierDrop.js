import React from 'react'
import CourierDropData from './CourierDropData';
import {get_courier} from '../actions/auth';
import {connect} from 'react-redux';

function openkuryer() {
    var x = document.getElementById("kuryerlerbody");
    if (x.style.display === "none") {
        x.style.display = "block";
    } else {
        x.style.display = "none";
    }
}


class CourierDrop extends React.Component {
  constructor(props) {
    super();
    props.get_courier();
  };    


  render() {   
    var arr = this.props.courier;
    if (!arr) return 'Loading';
    
    return(
        <div>
          <div onClick={openkuryer} className="kuryersecin">
            <div className="kuryerassignhead">
              <p className="assigncur">Kuriyer secin</p>
              <img className="kuryerheaddropdown" src="https://res.cloudinary.com/dgslkvls9/image/upload/v1602076412/Arrow.2_2_xlk8w2.png"></img>
            </div>
          </div>
          <div id="kuryerlerbody" className="kuryerlerlist">
            <div className="kuryersecimdetal">
              <CourierDropData courier={this.props.courier} />
            </div>   
          </div>
        </div>
    );
  }
}

export default connect(state => ({courier: state.auth.courier}), {get_courier}) (CourierDrop)

