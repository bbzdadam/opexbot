import React from 'react'
import IncomingD from './IncomingD'
import { incoming_list } from '../actions/auth'
import { connect } from 'react-redux';


class Incoming extends React.Component {

    

    constructor(props) {
        super();
        props.incoming_list();      
    }

    

    render() {

        let incomingdata = this.props.incoming;
        if (!incomingdata) return 'Loading';



        return(
            <div class="incoming">
                <h5>Incoming ({incomingdata.length})</h5>
                <div className="incominglist">
                    <IncomingD incoming={this.props.incoming}/>

		        </div>
            </div>
        )
    }
      
}

export default connect(state => ({incoming: state.auth.incoming}), {incoming_list})(Incoming)
