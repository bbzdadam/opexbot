import React from 'react'

class CompOrders extends React.Component {
    constructor(){
		super()
		this.state = {
			delivery: true	
		}
    }
    
    render(){

        let orderType

		if(this.state.delivery === true){
			orderType = "https://res.cloudinary.com/dgslkvls9/image/upload/v1598770300/delivery_3x_bbxxjy.png"
		} else {
			orderType = "https://res.cloudinary.com/dgslkvls9/image/upload/v1598770299/cars_3x_k9akn6.png"
		}

        return (
            <div class="completedorders">
		    	<p class="idnumberes">#{this.props.nid}</p>
		    	<p class="usermini">{this.props.fullname}</p>

		    	<div class="pickup">
                    <img src={orderType}></img>
		    	</div>
                
		    	<p class="ordertype">{this.props.type} in</p>
		    	<p class="timer">{this.props.time} min</p>
		    </div>
        )
    }
}

export default CompOrders