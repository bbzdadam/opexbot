import React from 'react'
import { NavLink } from 'react-router-dom';
import RestSwitch from './RestSwitch';


function myFunction() {
    document.getElementById("myDropdown").classList.toggle("show");
  }
  
  window.onclick = function(event) {
    if (!event.target.matches('.dropbtn')) {
      var dropdowns = document.getElementsByClassName("dropdown-content");
      var i;
      for (i = 0; i < dropdowns.length; i++) {
        var openDropdown = dropdowns[i];
        if (openDropdown.classList.contains('show')) {
          openDropdown.classList.remove('show');
        }
      }
    }
  }

class HeaderCard extends React.Component {

    constructor() {
        super()
        this.state = {
            isOpen: true
        }
    }


    render(){

        let restaurantStatus

        if (this.state.isOpen === true){
            restaurantStatus = "Restaurant Open"
        } else {
            restaurantStatus = "Restaurant Closed"
        }

        return(
            <div className="hdr">
                <div className="leftside">
                    <h5>Billing</h5>
                </div>
                <div className="rightside">
                <RestSwitch/>
                    {/* <button onClick={myFunction} className="dropbtn" ><span className="dot"></span> {restaurantStatus} <img src='https://res.cloudinary.com/dgslkvls9/image/upload/v1598770298/arrow-drop-down-material_lqvljb.png'></img> </button>
                    <div id="myDropdown" className="dropdown-content">
                        <a href="#"><span className="dot2"></span>{restaurantStatus}</a>
                    </div> */}
                    <a className="rght_bell" href="#"><img src="https://res.cloudinary.com/dgslkvls9/image/upload/v1598770300/notification_3x_jgoits.png"></img></a>
				    <NavLink to="/profile" ><a className="rght_profileicon" href="#"><img src="https://res.cloudinary.com/dgslkvls9/image/upload/v1598770300/icon_3x_anfphy.png"></img></a></NavLink>
                </div>

            </div>
        )
    }
}

export default HeaderCard