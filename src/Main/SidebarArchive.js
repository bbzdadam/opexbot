import React from 'react'
import { NavLink } from 'react-router-dom';

const Sidebar = () =>{
    return(
        <div className="sidebar">
            <NavLink to="/dashboard"><a><img id="mntp" src="https://res.cloudinary.com/dgslkvls9/image/upload/v1598958477/group_3x_hza5dn.png"></img></a></NavLink>
            <NavLink to="/app" ><img className="build-outline" src="https://res.cloudinary.com/dgslkvls9/image/upload/v1601534568/bulid-outline_3x_bvb7hg.png"></img></NavLink>
            <NavLink to="/dashboard" ><img className="build-outline" src="https://res.cloudinary.com/dgslkvls9/image/upload/v1598770299/cart-outline_3x_ctdo8b.png"></img></NavLink>
            <NavLink to="/statistics" ><img className="build-outline" src="https://res.cloudinary.com/dgslkvls9/image/upload/v1598770301/statistics_3x_vobtk6.png"></img></NavLink>
            <NavLink to="/card" ><img className="build-outline" src="https://res.cloudinary.com/dgslkvls9/image/upload/v1598770298/billing_3x_ragdwt.png"></img></NavLink>
            <NavLink to="/archive" ><img className="build-outline active" src="https://res.cloudinary.com/dgslkvls9/image/upload/v1601625555/Group_3x_picmg4.png"></img></NavLink>

            <a ></a>
	        <a><img class="bulid-outline " src="https://res.cloudinary.com/dgslkvls9/image/upload/v1598770301/question_3x_dhrqv0.png" id="bottom"></img></a>
        </div>
    )
}


export default Sidebar