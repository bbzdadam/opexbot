import React, { Component } from "react";
import Switch from "react-switch";
 
class RestSwitch extends Component {
  constructor() {
    super();
    this.state = { checked: true };
    this.handleChange = this.handleChange.bind(this);
  }
 
  handleChange(checked) {
    this.setState({ checked });
  }
 
  render() {
    return (
      <label>
        <span></span>
        <Switch 
        checked={this.state.checked}
        onChange={this.handleChange}
        offColor="#f9f9f9"
        offHandleColor="#db4646"
        onColor="#f9f9f9"
        onHandleColor="#03c56d"
        handleDiameter={30}
        uncheckedIcon={false}
        checkedIcon={false}
        boxShadow="0px 1px 5px rgba(0, 0, 0, 0.6)"
        activeBoxShadow="0px 0px 1px 10px rgba(0, 0, 0, 0.2)"
        height={20}
        width={48}
        className="react-switch"
        handleDiameter={20}
        id="material-switch"
        />
      </label>
    );
  }
}

export default RestSwitch