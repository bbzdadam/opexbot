import React from 'react'
import {connect} from 'react-redux'
import {checkAuthenticated} from '../src/actions/auth'
import { Redirect, Route } from 'react-router-dom'
 

export const PrivateRoute = () =>{

}

const mapStateProps = (state) => ({
  isAuthenticated: state.auth.uid
})