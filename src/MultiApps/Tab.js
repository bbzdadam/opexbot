import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import Menu from './Menu'

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`vertical-tabpanel-${index}`}
      aria-labelledby={`vertical-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `vertical-tab-${index}`,
    'aria-controls': `vertical-tabpanel-${index}`,
  };
}

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    backgroundColor: '#f7f7f9',
    
  },
  tabs: {
    width: '260px',
    fontFamily: 'Inter',
    textAlign: 'left',
    height: 936,
    textTransform: 'capitalize',
    
  },
}));

export default function VerticalTabs() {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);
  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  

  return (
    
    <div className={classes.root}style={{padding:"0px"}} >
        <div className="headmenu">
            <p className="Blmlr">Bölmələr</p>
        
            <Tabs
                orientation="vertical"
                variant="scrollable"
                value={value}
                onChange={handleChange}
                aria-label="Vertical tabs example"
                className={classes.tabs}
            >
                <Tab style={{borderBottom:"1px solid #e4e4e4"}} label="Ana Yemekler" {...a11yProps(0)} /> 
                <Tab style={{borderBottom:"1px solid #e4e4e4"}} label="Suplar" {...a11yProps(1)} />
                <Tab style={{borderBottom:"1px solid #e4e4e4"}} label="Salatlar" {...a11yProps(2)} />
                <Tab style={{borderBottom:"1px solid #e4e4e4"}} label="Qarnirler" {...a11yProps(3)} />
                <Tab style={{borderBottom:"1px solid #e4e4e4"}} label="Kabablar" {...a11yProps(4)} />
                <Tab style={{borderBottom:"1px solid #e4e4e4"}} label="Menyular" {...a11yProps(5)} />
                <Tab style={{borderBottom:"1px solid #e4e4e4"}} label="Pizzalar" {...a11yProps(6)} />  
                <Tab style={{borderBottom:"1px solid #e4e4e4"}} label="Burgerler" {...a11yProps(7)} />
                <Tab style={{borderBottom:"1px solid #e4e4e4"}} label="Donerler" {...a11yProps(8)} />
                <Tab style={{borderBottom:"1px solid #e4e4e4"}} label="Ickiler" {...a11yProps(9)} />
            </Tabs>
                
        </div> 
        
        <TabPanel value={value} index={0}>
          <Menu/>
        </TabPanel>
        <TabPanel value={value} index={1}>
          <Menu/>
        </TabPanel>
        <TabPanel value={value} index={2}>
          <Menu/>
        </TabPanel>
        <TabPanel value={value} index={3}>
          <Menu/>
        </TabPanel>
        <TabPanel value={value} index={4}>
          <Menu/>
        </TabPanel>
        <TabPanel value={value} index={5}>
          <Menu/>
        </TabPanel>
        <TabPanel value={value} index={6}>
          <Menu/>
        </TabPanel>  
        <TabPanel value={value} index={7}>
          <Menu/>
        </TabPanel>
        <TabPanel value={value} index={8}>
          <Menu/>
        </TabPanel>
        <TabPanel value={value} index={9}>
          <Menu/>
        </TabPanel> 
    </div>
  );
}