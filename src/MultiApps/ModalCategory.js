import React from 'react'
import {product_update} from '../actions/auth';
import {connect} from 'react-redux'

class ModalCategory extends React.Component {

    constructor(props) {
        super(); 
        this.state={
            name: "",
            ingridients: "",
            price: "",
            id: ""
        };


    }
    onChange = (e) => {
        this.setState({
            ...this.state,
            [e.target.name]: e.target.value 
        });
    }

    onSubmit = (e) => {
        e.preventDefault();
        this.props.product_update( this.state.name, this.state.ingridients, this.state.price, this.state.id);
        // alert("Submitted : " + this.state.username + this.state.surname + this.state.phone_number + this.state.id)
    }


    render() {
        return (
            <React.Fragment>
            {this.props.show && (
                <div>
                    <div id="editkuriyer" className="addmehsulpopup">
                        <div className="modalprof">
                            <div className="btnx">
                                <button className="closebt" onClick={this.props.onHide}><img className="clsbtnmg" src="https://res.cloudinary.com/dgslkvls9/image/upload/v1600776523/Add_Cirlce_Plus_4_3x_s4bc4m.png"></img></button>
                            </div>
                            
                                <div class="popuprectkuriyer">
                                <form onSubmit={(e) => this.onSubmit(e)}>
                                    <p classname="kuryerlihead">Yeni məhsul</p>
                                    <div classname="kuryercred">
                                        {/* <div className="mehsul_img_upd">
                                            <img className="mehsul_img_upd_icon" src="https://res.cloudinary.com/dgslkvls9/image/upload/v1600434247/Pen_Edit.8_2_h792xx.png"></img>
                                        </div> */}
                                        <input type="textarea" required name="name" value={this.state.name} onChange={(e) => this.onChange(e)} placeholder={this.props.mehsulname} className="panelprflinput"></input>
                                        <input type="textarea" required name="ingridients" value={this.state.ingridients} onChange={(e) => this.onChange(e)} placeholder={this.props.inqred} className="panelprflinput"></input>
                                        <input type="textarea" required name="price" value={this.state.price} onChange={(e) => this.onChange(e)} placeholder={this.props.price} className="panelprflinput"></input>
                                        <input className="idshow"  name="id" value={this.state.id } onChange={(e) => this.onChange(e)}   placeholder={this.props.id} type="textarea"  ></input>
                                    </div>
                                    <button type="submit" classname="elavekuryer">Yadda saxla</button>
                                    </form>
                                </div>
                            
                        </div>
                    </div>
                </div>
            )}
            </React.Fragment>
        );
    }
}

export default connect(null,{ product_update })(ModalCategory);