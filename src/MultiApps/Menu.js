import React from 'react'
import ModalCategory from './ModalCategory'

class Menu extends React.Component {

    state = {
        showModal: 0
    };
    
      getModal = value => {
        this.setState({ showModal: value });
    };
    
      hideModal = value => {
        this.setState({ showModal: 0 });
    };


    render() {
        
        return(
            <div>
            
                <div className="menubody">

                    <div className="menugrid">
                        
                    {this.props.show && this.props.mdata.map((mdata, key)=>  (
                        <div>
                        <div  onClick={() => this.getModal(mdata.id)} className="divmehsulelm">
                            <img className="mehsulsek" src="https://res.cloudinary.com/dgslkvls9/image/upload/v1601444886/burger_hgxvgv.jpg"></img>
                            <div className="mehsuldetal">
                                <p className="mehsulad">{mdata.name}</p>
                                <p className="mehsulinqr">{mdata.ingridients}</p>
                                <p className="mehsulqiymet">Qiymət : {mdata.price}m</p>
                            </div>
                        </div>

                        <ModalCategory 
                        show={this.state.showModal === mdata.id}
                        onHide={() => this.hideModal(mdata.id)}
                        mehsulname={mdata.name}
                        inqred={mdata.ingridients}
                        price={mdata.price}
                        id = {mdata.id}
                        />
                        </div>
                        
                        
                    ))}   
                        
                    </div>    

                    {/* <div className="menupage">
                        <img className="solox" src="https://res.cloudinary.com/dgslkvls9/image/upload/v1601450042/Arrow.8_1_hosynp.png"></img>
                        <p className="menupagenum">1</p>
                        <p className="menupagenum">2</p>
                        <p className="menupagenum">3</p>
                        <img className="sagox" src="https://res.cloudinary.com/dgslkvls9/image/upload/v1601450042/Arrow.8_2_dvmnvl.png"></img>    
                    </div>            */}
                </div>
            
            </div>        
        )
    }
}

export default Menu