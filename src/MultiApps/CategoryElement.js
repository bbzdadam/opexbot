import Menu from './Menu'
import React from 'react'


class CategoryElement extends React.Component {

    state = {
        showModal: 0
    };
    
      getModal = value => {
        this.setState({ showModal: value });
    };
    
      hideModal = value => {
        this.setState({ showModal: 0 });
    };

    render() {
        return (
            <div>
                {this.props.category.map((data, key)=> (
                    <div>
                        <div onClick={() => this.getModal(data.id)} className="category_body_categories">
                            <img className="category_icon" src="https://res.cloudinary.com/dgslkvls9/image/upload/v1600434247/Menu_Burger_Square_1_csphz9.png" ></img>      
                            <p className="category_nameself">{data.name}</p>
                        </div>
                
                    
                        <Menu show={this.state.showModal === data.id} mdata={data.mehsul}/>
                    </div>
                ))}
                
            </div>
        );
    }
}

export default CategoryElement