import React from 'react'
import SidebarBuild from '../Main/SidebarBuild'
import HeaderBuild from '../Main/HeaderBuild'
import Category from './Category'
import  Tab from './Tab'


const MultiApp = () => {

    return (
        <div className="adtapmadim">
            <SidebarBuild />
            <HeaderBuild />
            <div className="builderbody">
              <Category/>
            </div>  
        </div>
    );
}

export default MultiApp