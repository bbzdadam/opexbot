import React from 'react'
import CategoryElement from './CategoryElement';
import {categories} from '../actions/auth'
import {connect} from 'react-redux'

class Category extends React.Component {

  state = {
    post: [
      { id: 1, 
        name: "Ana yemekler",
        mehsul: [
          {
            id:1,
            mehsulname: "Burger",
            inqred: "Ketcup, Mayonez",
            price: 25,
          },
          {
            id:2,
            mehsulname: "Burger",
            inqred: "Ketcup, Mayonez",
            price: 25,
          },
          {
            id:3,
            mehsulname: "Burger",
            inqred: "Ketcup, Mayonez",
            price: 25,
          },
          {
            id:4,
            mehsulname: "Pizza",
            inqred: "Ketcup, Mayonez",
            price: 25,
          },
          {
            id:5,
            mehsulname: "Pizza",
            inqred: "Ketcup, Mayonez",
            price: 25,
          },
          {
            id:6,
            mehsulname: "Pizza",
            inqred: "Ketcup, Mayonez",
            price: 25,
          },
          {
            id:7,
            mehsulname: "Pizza",
            inqred: "Ketcup, Mayonez",
            price: 25,
          },
          {
            id:8,
            mehsulname: "Pizza",
            inqred: "Ketcup, Mayonez",
            price: 25,
          },
          {
            id:9,
            mehsulname: "Pizza",
            inqred: "Ketcup, Mayonez",
            price: 25,
          },
          {
            id:10,
            mehsulname: "Pizza",
            inqred: "Ketcup, Mayonez",
            price: 25,
          },
        ]

        },
    ]
  };
  


  constructor(props) {
    super();
    props.categories();     // bu categoryden list olaraq cekmek lazimdi bunda error cixmir   
}



render() {
    if (!this.props.category) return 'Loading';

    return (
      <div className="category_body">
        <div className="category_top">
          <p className="category_topname" >Bolmeler</p>
        </div>

        
        <CategoryElement category={this.props.category} />

      </div>

    );
  }
}

export default connect(state => ({category: state.auth.category}), {categories})(Category)
