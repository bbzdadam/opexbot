import axios from "axios";
import {
  SIGNUP_SUCCESS,
  SIGNUP_FAIL,
  LOGIN_SUCCESS,
  LOGIN_FAIL,
  ACTIVATION_SUCCESS,
  ACTIVATION_FAIL,
  RESET_PASSWORD_SUCCESS,
  RESET_PASSWORD_FAIL,
  RESET_PASSWORD_CONFIRM_SUCCESS,
  RESET_PASSWORD_CONFIRM_FAIL,
  LOGOUT,
  USER_LOADED_SUCCESS,
  USER_LOADED_FAIL,
  AUTHENTICATED_FAIL,
  AUTHENTICATED_SUCCESS,
  COURIER_LOADED_SUCCESS,
  COURIER_LOADED_FAIL,
  CCCREATE_SUCCESS,
  CCCREATE_FAIL,
  UPDATE_SUCCESS,
  UPDATE_FAIL,
  CATEGORY_SUCCESS,
  CATEGORY_FAIL,
  INCOMING_FAIL,
  INCOMING_SUCCESS,
  OUTGOING_FAIL,
  OUTGOING_SUCCESS,
  READY_FAIL,
  READY_SUCCESS,
  COURIER_UPDATE_SUCCESS,
  COURIER_UPDATE_FAIL,
  PRODUCT_UPDATE_SUCCESS,
  PRODUCT_UPDATE_FAIL,
  ADDTO_OUTGOING_SUCCESS,
  ADDTO_OUTGOING_FAIL,
} from "./types";

 //reactApiUrl = process.env('REACT_APP_API_URL')

export const checkAuthenticated = () => async (dispatch) => {
  if (typeof window == "undefined") {
    dispatch({
      type: AUTHENTICATED_FAIL,
    });
  }
  if (localStorage.getItem("access")) {
    const config = {
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
    };

    const body = JSON.stringify({ token: localStorage.getItem("access") });

    try {
      const res = await axios.post(
        `http://159.89.26.129/auth/jwt/verify/`,
        body,
        config
      );

      if (res.data.code !== "token_not_valid") {
        dispatch({
          type: AUTHENTICATED_SUCCESS,
        });
      } else {
        dispatch({
          type: AUTHENTICATED_FAIL,
        });
      }
    } catch (err) {
      dispatch({
        type: AUTHENTICATED_FAIL,
      });
    }
  } else {
    dispatch({
      type: AUTHENTICATED_FAIL,
    });
  }
};

export const load_user = () => async (dispatch) => {
  if (localStorage.getItem("access")) {
    const config = {
      headers: {
        "Content-Type": "application/json",
        'Authorization': `JWT ${localStorage.getItem("access")}`,
      },
    };

    try {
      const res = await axios.get(
        `http://159.89.26.129/api/user_info/`,
        config
      );
      dispatch({
        type: USER_LOADED_SUCCESS,
        payload: res.data,
      });
    } catch (err) {
      dispatch({
        type: USER_LOADED_FAIL,
      });
    }
  } else {
    dispatch({
      type: USER_LOADED_FAIL,
    });
  }
};

export const login = (email, password) => async (dispatch) => {
  const config = {
    headers: {
      "Content-Type": "application/json",
    },
  };

  const body = JSON.stringify({ email, password });

  try {
    const res = await axios.post(
      `http://159.89.26.129/auth/jwt/create/`,
      body,
      config
    );

    dispatch({
      type: LOGIN_SUCCESS,
      payload: res.data,
    });

    dispatch(load_user());
  } 
  catch (err) {
    dispatch({
      type: LOGIN_FAIL,
    });
  }
};

export const signup = ({ name, email, password, re_password }) => async (
  dispatch
) => {
  const config = {
    headers: {
      "Content-Type": "application/json",
    },
  };

  const body = JSON.stringify({ name, email, password, re_password });

  try {
    const res = await axios.post(
      `http://159.89.26.129/auth/users/`,
      body,
      config
    );

    dispatch({
      type: SIGNUP_SUCCESS,
      payload: res.data,
    });
  } catch (err) {
    dispatch({
      type: SIGNUP_FAIL,
    });
  }
};

export const verify = (uid, token) => async (dispatch) => {
  const config = {
    headers: {
      "Content-Type": "application/json",
    },
  };

  const body = JSON.stringify({ uid, token });

  try {
    const res = await axios.post(
      `http://159.89.26.129/auth/users/activation/`,
      body,
      config
    );

    dispatch({
      type: ACTIVATION_SUCCESS,
      payload: res.data,
    });
  } catch (err) {
    dispatch({
      type: ACTIVATION_FAIL,
    });
  }
};

export const reset_password = (email) => async (dispatch) => {
  const config = {
    headers: {
      "Content-Type": "application/json",
    },
  };

  const body = JSON.stringify({ email });

  try {
    const res = await axios.post(
      `http://159.89.26.129/auth/users/reset_password/`,
      body,
      config
    );

    dispatch({
      type: RESET_PASSWORD_SUCCESS,
      payload: res.data,
    });
  } catch (err) {
    dispatch({
      type: RESET_PASSWORD_FAIL,
    });
  }
};

export const reset_password_confirm = (
  uid,
  token,
  new_password,
  re_new_password
) => async (dispatch) => {
  const config = {
    headers: {
      "Content-Type": "application/json",
    },
  };

  const body = JSON.stringify({ uid, token, new_password, re_new_password });

  try {
    const res = await axios.post(
      `http://159.89.26.129/auth/users/reset_password_confirm/`,
      body,
      config
    );

    dispatch({
      type: RESET_PASSWORD_CONFIRM_SUCCESS,
      payload: res.data,
    });
  } catch (err) {
    dispatch({
      type: RESET_PASSWORD_CONFIRM_FAIL,
    });
  }
};

export const logout = () => (dispatch) => {
  dispatch({ type: LOGOUT });
};




export const get_courier = () => async (dispatch) => {
  if (localStorage.getItem("access")) {
    const config = {
      headers: {
        "Content-Type": "application/json",
        'Authorization': `JWT ${localStorage.getItem("access")}`,
      },
    };

    try {
      const res = await axios.get(
         `http://159.89.26.129/api/get_courier/`,
        config
      );
      dispatch({
        type: COURIER_LOADED_SUCCESS,
        payload: res.data,
      });
    } catch (err) {
      dispatch({
        type: COURIER_LOADED_FAIL,
      });
    }
  } else {
    dispatch({
      type: COURIER_LOADED_FAIL,
    });
  }
};







export const courier_create = (name, surname, phone_number) => async (dispatch) => {
  const config = {
    headers: {
      "Content-Type": "application/json",
      'Authorization': `JWT ${localStorage.getItem("access")}`,
    },
  };

  const body = JSON.stringify({ name, surname, phone_number });

  try {
    const res = await axios.post(
      `http://159.89.26.129/api/courier/create/`,
      body,
      config
    );

    dispatch({
      type: CCCREATE_SUCCESS,
      payload: res.data,
    });

    dispatch(load_user());
  } 
  catch (err) {
    dispatch({
      type: CCCREATE_FAIL,
    });
  }
};


export const profile_update = (name, surname, username) => async (dispatch) => {
  const config = {
    headers: {
      "Content-Type": "application/json",
      'Authorization': `JWT ${localStorage.getItem("access")}`,
    },
  };

  const body = JSON.stringify({ name, surname, username });

  try {
    const res = await axios.patch(
      `http://159.89.26.129/api/user_info_update/`,
      body,
      config
    );

    dispatch({
      type: UPDATE_SUCCESS,
      payload: res.data,
    });

    dispatch(load_user());
  } 
  catch (err) {
    dispatch({
      type: UPDATE_FAIL,
    });
  }
};


export const categories = () => async (dispatch) => {
  if (localStorage.getItem("access")) {
    const config = {
      headers: {
        "Content-Type": "application/json",
        'Authorization': `JWT ${localStorage.getItem("access")}`,
      },
    };

    try {
      const res = await axios.get(
         `http://159.89.26.129/api/get/`,
        config
      );
      dispatch({
        type: CATEGORY_SUCCESS,
        payload: res.data,
      });
    } catch (err) {
      dispatch({
        type: CATEGORY_FAIL,
      });
    }
  } else {
    dispatch({
      type: CATEGORY_FAIL,
    });
  }
};

export const incoming_list = () => async (dispatch) => {
  if (localStorage.getItem("access")) {
    const config = {
      headers: {
        "Content-Type": "application/json",
        'Authorization': `JWT ${localStorage.getItem("access")}`,
      },
    };

    try {
      const res = await axios.get(
         `http://159.89.26.129/get_incoming_list/`,
        config
      );
      dispatch({
        type: INCOMING_SUCCESS,
        payload: res.data,
      });
      

      
    } catch (err) {
      dispatch({
        type: INCOMING_FAIL,
      });
    }
  } else {
    dispatch({
      type: INCOMING_FAIL,
    });
  }
};

export const outgoing_list = () => async (dispatch) => {
  if (localStorage.getItem("access")) {
    const config = {
      headers: {
        "Content-Type": "application/json",
        'Authorization': `JWT ${localStorage.getItem("access")}`,
      },
    };

    try {
      const res = await axios.get(
         `http://159.89.26.129/get_outgoing_list/`,
        config
      );
      dispatch({
        type: OUTGOING_SUCCESS,
        payload: res.data,
      });
    } catch (err) {
      dispatch({
        type: OUTGOING_FAIL,
      });
    }
  } else {
    dispatch({
      type: OUTGOING_FAIL,
    });
  }
};

export const ready_list = () => async (dispatch) => {
  if (localStorage.getItem("access")) {
    const config = {
      headers: {
        "Content-Type": "application/json",
        'Authorization': `JWT ${localStorage.getItem("access")}`,
      },
    };

    try {
      const res = await axios.get(
         `http://159.89.26.129/get_ready_list/`,
        config
      );
      dispatch({
        type: READY_SUCCESS,
        payload: res.data,
      });
    } catch (err) {
      dispatch({
        type: READY_FAIL,
      });
    }
  } else {
    dispatch({
      type: READY_FAIL,
    });
  }
};


export const courier_update = (username, surname, phone_number, id) => async (dispatch) => {
  const config = {
    headers: {
      "Content-Type": "application/json",
      'Authorization': `JWT ${localStorage.getItem("access")}`,
    },
  };

  const body = JSON.stringify({ name: username,surname, phone_number, id });

  try {
    const res = await axios.put(
      `http://159.89.26.129/api/courier/${id}/update/`,
      body,
      config
    );

    dispatch({
      type: COURIER_UPDATE_SUCCESS,
      payload: res.data,
    });

    dispatch(get_courier());
  } 
  catch (err) {
    dispatch({
      type: COURIER_UPDATE_FAIL,
    });
  }
};


export const product_update = (name, ingridients, price, id) => async (dispatch) => {
  const config = {
    headers: {
      "Content-Type": "application/json",
      'Authorization': `JWT ${localStorage.getItem("access")}`,
    },
  };

  const body = JSON.stringify({ name, ingridients, price, id });

  try {
    const res = await axios.put(
      `http://159.89.26.129/api/products/${id}/update/`,
      body,
      config
    );

    dispatch({
      type: PRODUCT_UPDATE_SUCCESS,
      payload: res.data,
    });

    dispatch(categories());
  } 
  catch (err) {
    dispatch({
      type: PRODUCT_UPDATE_FAIL,
    });
  }
};


export const incomingto_out = (id) => async (dispatch) => {
  const config = {
    headers: {
      "Content-Type": "application/json",
      'Authorization': `JWT ${localStorage.getItem("access")}`,
    },
  };
 const body = JSON.stringify(id);

  try {
    const res = await axios.put(
      `http://159.89.26.129/add_to_outgoing/${id}/`,
      body,
      config
    );

    dispatch({
      type: ADDTO_OUTGOING_SUCCESS,
      payload: res.data,
    });

    
  } 
  catch (err) {
    dispatch({
      type: ADDTO_OUTGOING_FAIL,
    });
  }
};
