import React, { Component } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Sidebar from './Main/Sidebar'
import Dashboard from './Dashboard/Dashboard';
import Statistics from './Statistics/StatisticsApp';
import MultiApp from './MultiApps/MultiApp';
import Card from './Card/CardApp';
import Profilepanel from './Profile/Profilepanel'
import Archive from './Archive/Archive';
import Login from './Login/Login'
import NotFound from './Login/NotFound';
import ForgotPassword from './Login/ForgotPassword';
import Success from './Login/Success';
import SuccessPassword from './Login/SuccessPassword';
import ResetPassword from './Login/ResetPassword';
import PrivateRoute from './PrivateRoute'
import { Provider } from 'react-redux';
import store from './store';
  

 
class App extends Component {
  render() {
    return (      
      <Provider store={store}>
        <BrowserRouter>
          
            <Switch>
              <Route path="/card" component={Card} />
              <Route path="/" component={Login} exact/>
              <Route path="/dashboard" component={Dashboard}/>
              <Route path="/statistics" component={Statistics}/>
              <Route path="/app" component={MultiApp}/>
              <Route path="/profile" component={Profilepanel} />
              <Route path="/archive" component={Archive} />
              <Route path="/forgetpass" component={ForgotPassword}/>
              <Route path="/mailrecoverysuccess" component={Success}/>
              <Route path="/resetpasswordconfirm" component={ResetPassword} />
              <Route path="/passwordchangesucced" component={SuccessPassword} />
              <Route component={NotFound}/>
              
            </Switch>
          
        </BrowserRouter>
      </Provider>
    );
  }
}
 
export default App;